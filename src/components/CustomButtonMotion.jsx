import { motion } from 'framer-motion'
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';


function CustomButtonMotion({ img, text, classNames,onClick, to, y }) {
	const [isTapped, setIsTapped] = useState(false);
	const  nav  = useNavigate()

	return (
		<Link
		to={to}
		className="cursor-default"
		onClick={onClick}
		>
			<motion.div
				onMouseDown={() => setIsTapped(true)}
				onMouseUp={() => setIsTapped(false)}
				className={classNames+" cursor-pointer"}
				style={{ backgroundImage: "linear-gradient(90deg, #4D9592 0%, #66C794 100%" }}
				whileHover={{ backgroundImage: "linear-gradient(90deg, rgba(219,92,110,1) 0%, rgba(216,137,116,1) 100%" }}
				whileTap={{ scale: 0.9 }}
				transition={{ duration: 0.2, type: "spring", stiffness: 100 }}
			>
				{text}
				<motion.img
					className={text.toLowerCase() === "login" ? "md:h-4" :"h-2 md:h-4 pl-2"}
					src={img}
					alt=""
					animate={isTapped ? ((y)?{y:10}:{ x: 10 }) : {}}
				/>
			</motion.div>

		</Link>
	)
}

export default CustomButtonMotion
