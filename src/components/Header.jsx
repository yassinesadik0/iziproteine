import menu from "../assets/icons/menu-02.svg";
import icon from "../assets/icons/arrow-login.svg";
import { useState } from "react";
import { XMarkIcon } from "@heroicons/react/24/outline";
import { Dialog } from "@headlessui/react";
import logo from "../assets/hero/log.svg";
import cart from "../assets/products/Iconshooping.svg";
import Cart from "./cart/cart";
import { Link } from "react-router-dom";
import React from "react";
import { HashLink } from "react-router-hash-link";
import DropDown from "./DropDown";
import { NavLink } from "react-router-dom";

const navigation = [
  { name: "Accueil", to: "/" },
  { name: "Produits", to: "/products" },
  { name: "Actualités & Press", to: "/blog" },
  { name: "Notre équipe", to: "#team" },
  { name: "Contact", to: "#contact" },
];

function Header() {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [cartOpen, setCartOpen] = useState(false);
//   const [isAuth, setIsAuth] = useState(false);
  const user = JSON.parse(localStorage.getItem("user"));

  return (
    <div className="px-6 pt-6 md:px-20 lg:px-32 z-10 lg:mb-12">
      <nav className="flex items-center justify-between" aria-label="Global">
        <div className="flex lg:flex-1">
          <Link to="/" className="-m-1.5 p-1.5">
            <span className="sr-only">Your Company</span>
            <img className="h-8 md:h-12" src={logo} alt="Izi Proteine logo" />
          </Link>
        </div>
        <div className="flex xl:hidden">
          <button
            type="button"
            className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-600"
            onClick={() => setMobileMenuOpen(true)}
          >
            <span className="sr-only">Open main menu</span>
            <img src={menu} alt="menu" />
          </button>
        </div>
        <div className="hidden xl:flex xl:items-center xl:gap-x-12">
          {navigation.map((item) => {
            if (item.name.toLowerCase() === "contact")
              return (
                <HashLink
                  key={item.name}
                  smooth
                  className="text-sm font-semibold leading-6 text-secondary-800"
                  to={`/${item.to}`}
                >
                  {item.name}
                </HashLink>
              );
            else if (item.name.toLowerCase() === "notre équipe")
              return (
                <HashLink
                  key={item.name}
                  smooth
                  className="text-sm font-semibold leading-6 text-secondary-800"
                  to={`/${item.to}`}
                >
                  {item.name}
                </HashLink>
              );
            else
              return (
                <NavLink
                  key={item.name}
                  to={item.to}
                  className="text-sm font-semibold leading-6 text-secondary-600"
                >
                  {item.name}
                </NavLink>
              );
          })}
        </div>
        <div className="hidden xl:flex xl:flex-1 items-center  xl:justify-end space-x-4">
          {user && <DropDown role={user.role} />}
          <button
            id="cart"
            onClick={() => setCartOpen(true)}
            className="w-10  flex flex-row justify-end items-center h-10 pl-2"
          >
            <img id="cart" src={cart} alt="" />
          </button>
        </div>
      </nav>
      <Cart visible={cartOpen} closeHandler={() => setCartOpen(false)} />
      <Dialog
        as="div"
        open={mobileMenuOpen && !cartOpen}
        onClose={() => setMobileMenuOpen(false)}
      >
        <Dialog.Panel
          focus="true"
          className={`fixed inset-0 z-20 overflow-y-auto bg-white px-6 py-6 lg:hidden`}
        >
          <div className="flex items-center justify-between">
            <Link to="/" className="-m-1.5 p-1.5">
              <span className="sr-only">Your Company</span>
              <img className="h-8" src={logo} alt="" />
            </Link>
            <button
              type="button"
              className="-m-2.5 rounded-md p-2.5 text-secondary-600"
              onClick={() => setMobileMenuOpen(false)}
            >
              <span className="sr-only">Close menu</span>
              <XMarkIcon className={`h-6 w-6 `} aria-hidden="true" />
            </button>
          </div>
          <div className="mt-6 flow-root">
            <div className="-my-6 divide-y divide-secondary-500/10">
              <div className="space-y-2 py-6">
                {navigation.map((item) => (
                  <Link
                    key={item.name}
                    to={item.to}
                    className="-mx-3 block rounded-lg py-2 px-3 text-base font-semibold leading-7 text-secondary-800 hover:bg-secondary-400/10"
                  >
                    {item.name}
                  </Link>
                ))}
                <button onClick={() => setCartOpen(!cartOpen)}>
                  <img src={cart} alt="" />
                </button>
              </div>
            </div>
          </div>
        </Dialog.Panel>
      </Dialog>
    </div>
  );
}

export default Header;
