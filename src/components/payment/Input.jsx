import React from 'react'

function Input({ type, name, placeholder, error,onChange, ...rest }) {

	return (
		<input
			className={`${error ? "border-2 border-red-500" : "border border-gray-200"} appearance-none block w-full bg-gray-200 text-secondary-500   rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm`}
			id={name}
			name={name}
			type={type}
			placeholder={placeholder ? placeholder : ""}
			onChange={onChange}
			{...rest}
		/>
	)
}

export default Input
