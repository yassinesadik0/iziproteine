import React, { useState, useRef, useEffect } from "react";
import Input from "./Input";
import AdminSubHeader from "./admin/AdminSubHeader";
import axios from "axios";
// import Navbar from "components/Navbars/AuthNavbar.js";
// import Footer from "components/Footers/Footer.js";
// import AdminSideBar from "./admin/AdminSideBar";
import icon from "../assets/icons/icon.png";

export default function Profile() {
	const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")))
	const [errors, setErrors] = useState(null)
	const [stats, setStats] = useState(null)
	const firstName = useRef(null)
	const lastName = useRef(null)
	const email = useRef(null)
	const telephone = useRef(null)
	const address = useRef(null)
	const password = useRef(null)
	const role = useRef(null)
	const photo = useRef(null)
	const stattics = { id: user?.id };

	useEffect(() => {
		if (user.role.toLowerCase() === "supplier") {
			axios.post(`http://127.0.0.1:8000/api/v1/privateWasteStatistic`, stattics, {
				headers: {
					Authorization:
						"Bearer " + localStorage.getItem("authToken"),
				},
			})
				.then(response => {
					setStats(response.data)
				})
				.catch(error => {
					setErrors(error.response?.data.errors);
				});
		}
	}, [])

	return (
		<>

			{/* <Navbar transparent /> */}
			<main className="profile-page">
				<section className="relative block h-500-px">
					<div
						className="absolute top-0 w-full h-full bg-center bg-cover"
						style={{
							backgroundImage:
								'url("https://images.unsplash.com/photo-1499336315816-097655dcfbda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2710&q=80")'
						}}
					>
						<span
							id="blackOverlay"
							className="w-full h-full absolute opacity-50 bg-black"
						/>
					</div>
					<div
						className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px"
						style={{ transform: "translateZ(0px)" }}
					>
						<svg
							className="absolute bottom-0 overflow-hidden"
							xmlns="http://www.w3.org/2000/svg"
							preserveAspectRatio="none"
							version="1.1"
							viewBox="0 0 2560 100"
							x={0}
							y={0}
						>
							<polygon
								className="text-slate-200 fill-current"
								points="2560 0 2560 100 0 100"
							/>
						</svg>
					</div>
				</section>
				<section className="relative py-16">
					<div className="container mx-auto px-4">
						<div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
							<div className="px-6">
								<div className="flex flex-wrap justify-center">
									<div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
										<div className="relative">
											<img
												alt="..."
												src={icon}
												className="shadow-xl rounded-full h-auto align-middle border-none absolute -m-16 -ml-20 lg:-ml-16 max-w-150-px"
											/>
										</div>
									</div>
									<div className="w-full lg:w-4/12 px-4 lg:order-3 lg:text-right lg:self-center">
									</div>
								</div>
								<div className="text-center mt-12">
									<h3 className="text-4xl font-semibold leading-normal mb-2 text-slate-700">
										{
											"Izi Proteine"
										}
									</h3>
									<div className="text-sm leading-normal mt-0 mb-2 text-slate-400 font-bold uppercase">
										<i className="fas fa-envelope mr-2 text-lg text-slate-400" />
										{
											"iziproteine@gmail.com"
										}
									</div>
									<div className="mb-2 text-slate-600 mt-10">
										<i className="fas fa-user-tie mr-2 text-lg text-slate-400" />
										{
											"admin"
										}
									</div>
									<div className="mb-2 text-slate-600">
										<i className="fas fa-university mr-2 text-lg text-slate-400" />
										Cite d'innovation
									</div>
								</div>
								<div className="mt-10 py-10 border-t border-slate-200 text-center">
									<div className="flex flex-wrap justify-center">
										<div className="w-full lg:w-9/12 px-4">
											<p className="mb-4 text-lg leading-relaxed text-slate-700">

												Welcome to the admin profile! We're excited to have you on board. As an admin,
												you'll have access to a powerful toolset to navigate and manage various aspects of our organization. 
												The sidebar will be your guide, 
												allowing you to effortlessly navigate through the different sections and efficiently carry out your administrative tasks.
												 We're confident that your skills and expertise will contribute greatly to our team. Once again, 
												 welcome, and we look forward to your valuable contributions!
											</p>
										</div>
									</div>
								</div>
								{
									user.role.toLowerCase() === "supplier" ? <AdminSubHeader
										c1={"Déchets livrés"}
										c2={"CO2 reduction"}
										c3={"déposé"}
										stats={stats?.stats}
									/> : ""
								}

							</div>
						</div>
					</div>
				</section>
			</main>

			{/* <Footer /> */}
		</>
	);
}
