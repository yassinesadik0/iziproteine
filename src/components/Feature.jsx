import { motion } from "framer-motion";


function Feature(props) {

	// const fContainer = {
	// 	hidden: { opacity: 1, scale: 0 },
	// 	visible: {
	// 		opacity: 1,
	// 		scale: 1,
	// 		transition: {
	// 			delayChildren: 0.4,
	// 			staggerChildren: 0.3
	// 		}
	// 	}
	// };

	// const fItem = {
	// 	hidden: { y: 20, opacity: 0 },
	// 	visible: {
	// 		y: 0,
	// 		opacity: 1
	// 	}
	// };

	return (
		<motion.div
			className="flex flex-col justify-center items-start p-10 gap-8 lg:w-[22rem]"
			// variants={fContainer}
			// initial="hidden"
			// whileInView="visible"
		>
			<motion.img className="w-10 lg:w-16" src={props.data.img} alt=""  />
			<motion.div className="flex flex-col justify-center items-start gap-4 "   >
				<motion.h3 className="font-semibold text-sm md:text-lg  text-secondary-700 "  >
					{props.data.heading}
				</motion.h3>
				<motion.p className="font-normal text-xs md:text-sm text-secondary-400 "   >
					{props.data.paragraph}
				</motion.p>
			</motion.div>
		</motion.div>
	);
}

export default Feature;