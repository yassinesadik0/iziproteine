import React, { useEffect, useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import useAuth from "../custom_hooks/useAuth";

const WithAuth = () => {

	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		setTimeout(() => {
			setIsLoading(false);
		}
			, 1000);

	}, []);

	return (
		<>
			{
				isLoading ? <div className="flex items-center justify-center min-h-screen">
					<div
						style={{ borderTopColor: "transparent" }}
						className="w-8 h-8 border-4 font-semibold border-secondary-400 rounded-full animate-spin"
					/>
					<p className="ml-2 border-secondary-600 font-semibold">LOADING...</p>
				</div> : <Outlet /> 
			}
		</>
	);
};

export default WithAuth;