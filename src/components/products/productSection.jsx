import React, { useEffect, useRef, useState } from "react";
import ProductImg from "../../assets/products/dummy-product.png";
import Bag from "../../assets/products/bag.png";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import shoping_bag from "../../assets/icons/shopping-bag-01.svg"
import starFilled from "../../assets/icons/star-filled.svg"
import starEmpty from "../../assets/icons/star-empty.svg"
import glow_bg from "../../assets/addons/Vectorimage.png"
import { useDispatch } from "react-redux";
import { add } from "../../redux/cartReducer";
import { motion } from "framer-motion";
import bagPic from "../../assets/products/bag.png"
import secondBag from "../../assets/products/dummy-product.png"
// import { add } from "../../redux/cartReducer";

// const photos = [
// 	bagPic,
// 	secondBag

// ]

export default function ProductSection({ reviews, stock, description, name, prevPrice, variants, reversed, id, photos }) {
	const dispatch = useDispatch()

	const [price, setPrice] = useState(variants[0].price)
	const [variant, setVariant] = useState(0)

	// init state of the reviews stars
	const [stars, setStars] = useState([
		<img key={0} src={starEmpty} alt="" />,
		<img key={1} src={starEmpty} alt="" />,
		<img key={2} src={starEmpty} alt="" />,
		<img key={3} src={starEmpty} alt="" />,
		<img key={4} src={starEmpty} alt="" />,
	])

	return (
		<div className="relative">
			<section className={`flex flex-col  ${(reversed) ? "lg:flex-row-reverse" : "lg:flex-row"}  p-1.5 bg-transparent max-w-screen-xl mb-40 mt-40 bg-no-repeat bg-right px-4 py-8 mx-auto md:gap-20 md:py-16 md:grid-cols-12`}
			>
				<div className=" flex justify-center mx-auto gap-5 items-center lg:w-3/6 w-5/6 mb-4 ">
					<Swiper
						modules={[Navigation]}
						spaceBetween={2}
						slidesPerView={1}
						speed={500}
						loop={true}
						navigation={true}
						className="w-600 flex justify-center h-fit z-40 bg-transparent "
						style={{ backgroundColor: "transparent" }}
					>
						{photos.map((e, index) => <SwiperSlide style={{ backgroundColor: "transparent" }} className="rounded-lg " key={index}>
							<img key={index} className="w-2/4 mx-auto" src={e.file_path} alt="" />
						</SwiperSlide>)}
					</Swiper>
				</div>
				<motion.div
					initial={{ y: -100, opacity: 0 }}
					whileInView={{ y: 0, opacity: 1 }}
					transition={{ duration: 1 }}

					className="lg:w-3/6 w-fit h-2/4 md:h-auto shadow-lg mx-auto bg-gray-100 z-40 backdrop-filter backdrop-blur-sm bg-opacity-30 p-10 gap-3 max-w-fit  flex flex-col rounded-xl">
					<div className="inline py-3 gap-0 relative w-fit" ><p className="md:text-6xl text-4xl font-bold">{name} <sup className="font-light text-xl absolute inset-0 left-full">TM</sup></p></div>
					<div className="text-base md:text-lg h-3/4 flex justify-center flex-col">
						<p>{description}</p>
					</div>
					<div className=" h-full flex flex-col justify-end gap-1 md:gap-3.5">
						<div className="flex flex-row ">
							<div className={`${(stock < 20) ? ((stock < 10) ? ((stock === 0) ? "hidden" : "bg-danger-500 px-2.5") : "bg-warning-500 px-1.5") : "bg-success-500 px-1.5"} rounded-full w-fit p-1`}>
								<h1>{stock}</h1>
							</div>
							<p className={`mt-1 ml-1 ${(stock === 0) ? "hidden" : ""} text-lg`}>items in stock!</p>
							<p className={`mt-1 ml-1 text-danger-500 font-bold ${(stock !== 0) ? "hidden" : ""} text-lg`}>item out of stock!</p>
						</div>
						{/* <div className="flex flex-row gap-2">
							{
								stars.map((e, index) => {
									if (index + 1 <= reviews) {
										return <img key={index} src={starFilled} />;
									}
									return e;
								})
							}
						</div> */}
						<label for="countries_disabled" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Select an option</label>
						<select onChange={e => { setPrice(variants[e.currentTarget.value].price); setVariant(e.currentTarget.value) }} className="bg-transparent border-2 border-gray-400 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-1/4 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
							{
								variants.map((e, index) => <option value={index}>{e.weight >= 1000 ? `${e.weight / 1000}kg` : `${e.weight}g`}</option>)
							}
						</select>
						<h3 className="text-lg text-success-900 font-bold">{price}DH<strike className={`ml-2 font-light ${(!prevPrice) ? "hidden" : ""} text-gray-400`}>{prevPrice}DH</strike></h3>
						<button onClick={() => dispatch(add({ id: id, name: name, variant: `${variants[variant].weight >= 1000 ? `${variants[variant].weight / 1000}kg` : `${variants[variant].weight}g`}`, image: photos[0].file_path ,price: price, variant_id: variants[variant].id, product_id: id }))} disabled={(stock === 0) ? true : false} className={`${(stock === 0) ? 'bg-gray-400' : "bg-primary-600 hover:bg-opacity-80"}  lg:w-2/6 p-3 sm:pl-6 rounded-md text-white flex justify-center w-fit items-center`}><span className="hidden sm:block">Add To Cart</span><img className="mx-auto" src={shoping_bag} /></button>
					</div>
				</motion.div>

			</section>
			<img className={`absolute 
                            lg:-top-[10%] ${(!reversed) ? "lg:left-[40%]" : "lg:right-[40%]"} lg:w-3/5
                            md:top-[40%] md:left-[0%]  
                            top-[40%] mobile:left-[0%]
                            w-full  backdrop:bg-blur-md h-auto mx-auto block opacity-50 `} src={glow_bg} alt="" />
		</div>
	)
}