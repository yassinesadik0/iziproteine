import React from "react";
import productImg from "../../assets/products/dummy-product.png"
import line from "../../assets/addons/line.svg"
import { motion } from "framer-motion"
import CustomButtonMotion from "../CustomButtonMotion";
import arrow from "../../assets/icons/down-arrow.svg"





export default function Hero() {



    return (

        <section className="flex justify-center ">
            {/* <div className="container border">
                <div className="pl-20 md:pl-0 w-full border flex items-start gap-10 flex-col">

                    <motion.h1 initial={{ y: 50, opacity: 0 }} animate={{ y: 0, opacity: 1 }} transition={{ duration: 1 }} className="text-center flex flex-col md:flex-row md:text-6xl text-4xl font-bold text-secondary-700">Our latest<span className="relative w-fit flex flex-col pl-2 text-secondary-400 text-4xl md:text-6xl font-bold">Product</span></motion.h1>

                    <div className=" border ">
                        <p className="max-w-2xl mx-0 ltext-center mb-6 font-light text-gray-500 lg:mb-8 md:text-lg lg:text-xl dark:text-gray-400">Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, vitae. Autem, explicabo dolor exercitationem quasi amet temporibus modi laborum dignissimos? Similique, quos vitae laborum fugit veniam eveniet eos dolor quas?</p>

                        <CustomButtonMotion y classNames="lg:w-1/4 w-fit rounded-lg text-white font-bold p-4 gap-2 flex flex-row justify-center items-center" text="Browse" img={arrow} />
                    </div>
                </div>


            </div> */}
            <div class=" container flex flex-col justify-center items-center gap-10 py-24 my-[5vw] px-6 text-center dark:bg-neutral-900">
                <motion.h1 initial={{ y: 50, opacity: 0 }} animate={{ y: 0, opacity: 1 }} transition={{ duration: 1 }} className="text-center flex flex-col md:flex-row md:text-6xl lg:text-9xl text-4xl font-bold text-secondary-700">Découvrez Nos<span className="relative w-fit flex flex-col pl-2 text-secondary-400 text-4xl md:text-6xl lg:text-9xl font-bold">Produits</span></motion.h1>
                <p className="max-w-full mx-0 ltext-center mb-6 font-medium text-gray-500 lg:mb-8 md:text-lg lg:text-xl dark:text-gray-400">
                    Ce qui distingue nos produits, ce n'est pas seulement leur caractère respectueux de l'environnement, mais aussi leur teneur élevée en protéines. Nous nous sommes associés à des experts de renom pour développer des méthodes de pointe permettant d'extraire des protéines à partir de sources naturelles présentes dans les déchets verts. Ces protéines sont ensuite intégrées à notre gamme de produits, vous offrant une solution durable et riche en nutriments.
                </p>
                <CustomButtonMotion y classNames="lg:w-[20vw] w-fit rounded-lg text-white font-bold p-4 gap-2 flex flex-row justify-center items-center" text="Browse"
                onClick={() => {
                    window.scrollTo({
                        top: 1000,
                        behavior: 'smooth'
                    });
                }}
                img={arrow} />

            </div>
        </section >





    )



}