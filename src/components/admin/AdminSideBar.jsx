/*eslint-disable*/
import React, { useEffect, useRef, useState } from "react";
import { Link, Navigate, useLocation, useNavigate } from "react-router-dom";
import logo from '../../assets/hero/log.svg';
import open from "../../assets/icons/menu-02.svg"
import close from "../../assets/icons/x.svg"
import product from "../../assets/icons/product.svg"
import supplier from "../../assets/icons/supplier.svg"
import waste from "../../assets/icons/waste.svg"
import wasteType from "../../assets/icons/wasteType.svg"
import home from "../../assets/icons/home.svg"
import variants from "../../assets/icons/variants.svg"
import post from "../../assets/icons/post.svg"
import role from "../../assets/icons/role.svg"
import users from "../../assets/icons/users.svg"
import discount from "../../assets/icons/discount.svg"
import delivery from "../../assets/icons/delivery.svg"
import logout from "../../assets/icons/logout.svg"
import settings from "../../assets/icons/settings.svg"
import user from "../../assets/icons/user.svg"
import { usePostLogOutMutation } from "../../redux/apiSlice";


const managementNavigation = [
	{
		name: "Home",
		to: "/admin",
		icon: home,
	},
	{
		name: "Products",
		to: "/admin/products",
		icon: product,
	},
	{
		name: "Variants",
		to: "/admin/variants",
		icon: variants,
	},
	{
		name: "Blog Posts",
		to: "/admin/blog",
		icon: post,
	},
	{
		name: "Waste Types",
		to: "/admin/wasteTypes",
		icon: wasteType,
	},
	{
		name: "Waste",
		to: "/admin/waste",
		icon: waste,
	},
	{
		name: "Orders",
		to: "/admin/orders",
		icon: delivery,
	},
]

const profileNavigation = [
	{
		name: "profile",
		to: "/admin/profile",
		icon: user,
	}
	// {
	// 	name: "logout",
	// 	to: "/admin/logout",
	// 	icon: logout,
	// },
]


export default function AdminSideBar() {

	let url = useLocation()
	const active = useRef(null)
	const updatedUrl = useRef(url.pathname)
	const [logoutResponse, setLogoutResponse] = useState(null)
	const nav = useNavigate()

	const handleLogOut = (e) => {
		e.preventDefault()
		localStorage.getItem("authToken") && localStorage.getItem("user") &&
			localStorage.removeItem("authToken")
		localStorage.removeItem("user")
		nav("/login")
		
	}


	useEffect(() => {
		updatedUrl.current = url.pathname
	}, [url.pathname]);

	const clicked = (e, url) => {
		if (active.current !== null) {
			active.current.classList.remove("bg-primary-200")
		}
		active.current = e.target
		active.current.className += " bg-primary-200"
	}

	if (logoutResponse?.status == "logout") {
		return <Navigate to={"/"} />
	}

	return (
		<>
			<nav className="md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden shadow-xl bg-white flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 px-6">
				<div className="md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto">
					<Link to="/" className="-m-1.5 p-1.5">
						<span className="sr-only">Your Company</span>
						<img className="h-8 md:h-12" src={logo} alt="Izi Proteine logo" />
					</Link>

					<button
						className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
						type="button"
					>
						<img src={open} alt="" />
					</button>

					{/* Collapse */}
					<div
						className={
							"md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded"
						}
					>
						{/* Collapse header */}
						<div className="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-blueGray-200">
							<div className="flex flex-wrap">
								<div className="w-6/12">
									<Link to="/" className="-m-1.5 p-1.5">
										<span className="sr-only">Your Company</span>
										<img className="h-8 md:h-12" src={logo} alt="Izi Proteine logo" />
									</Link>
								</div>
								<div className="w-6/12 flex justify-end">
									<button
										type="button"
										className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
									>
										<img src={close} alt="" />
									</button>
								</div>
							</div>
						</div>

						{/* Divider */}
						<hr className="my-4 md:min-w-full" />
						{/* Heading */}
						<h6 className="md:min-w-full text-blueGray-500 text-xs uppercase font-bold block pt-1 pb-4 no-underline">
							Management
						</h6>
						{/* Navigation */}

						<ul className="md:flex-col md:min-w-full flex flex-col list-none">
							{
								managementNavigation.map((item, index) => <li key={index} className="items-center">
									<Link
										onClick={(e) => clicked(e, updatedUrl.current)}
										className={
											"a text-xs uppercase py-3 font-bold block text-secondary-500 hover:bg-secondary-200 px-4 rounded-lg active:bg-secondary-300"
										}
										to={item.to}
									>
										<img
											className={"inline mr-2"}
											src={item.icon}
										/>{item.name}
									</Link>
								</li>)
							}
						</ul>
						<hr className="my-4 md:min-w-full" />
						<h6 className="md:min-w-full text-blueGray-500 text-xs uppercase font-bold block pt-1 pb-4 no-underline">
							Profile
						</h6>
						<ul className="md:flex-col md:min-w-full flex flex-col list-none">
							{
								profileNavigation.map((item, index) => <div key={index}><li className="items-center">
									<Link
										onClick={(e) => clicked(e, updatedUrl.current)}
										className={
											"a text-xs uppercase py-3 font-bold block text-secondary-500 active:bg-secondary-200 hover:bg-secondary-200 px-4 rounded-lg"
										}
										to={item.to}
									>
										<img
											className={"inline mr-2"}
											src={item.icon}
										/>{item.name}
									</Link>
								</li>
									<hr className="my-4 md:min-w-full" />
								</div>)
							}
							<li>
								<form onSubmit={handleLogOut}>
									<button
										className={
											"a text-xs uppercase py-3 font-bold block text-secondary-500 active:bg-secondary-200 hover:bg-secondary-200 px-4 rounded-lg"
										}
										type="submit"
									>
										<img
											className={"inline mr-2"}
											src={logout}
										/>{"logout"}
									</button>
								</form>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</>
	);
}
