import React from "react";
import CardStats from "./CardStats";

export default function AdminSubHeader({ stats, c1, c2, c3 }) {
	return (
		<>
			{/* Header */}
			<div className="relative bg-text-gradient to-right md:pt-32 pb-32 pt-12" >
				<div className="px-4 md:px-10 mx-auto w-full">
					{/* Card stats */}
					<div className="flex flex-wrap">
						<div className="w-full lg:w-4/12 xl:w-4/12 px-4">
							<CardStats
								statSubtitle={c1 ? c1 : "TRAFFIC"}
								statTitle={stats ? stats.wasteTreated : "0"}
								statIconName="far fa-chart-bar"
								statIconColor="bg-red-500"
							/>
						</div>
						<div className="w-full lg:w-4/12 xl:w-4/12 px-4">
							<CardStats
								statSubtitle={c2 ? c2 : "NEW USERS"}
								statTitle={stats ? stats.co2Reduced : "0"}
								statIconName="fas fa-chart-pie"
								statIconColor="bg-orange-500"
							/>
						</div>
						<div className="w-full lg:w-4/12 xl:w-4/12 px-4">
							<CardStats
								statSubtitle={c3 ? c3 : "SALES"}
								statTitle={stats ? stats.deposited : "0"}
								statIconName="fas fa-users"
								statIconColor="bg-pink-500"
							/>
						</div>

					</div>
				</div>
			</div>
		</>
	);
}
