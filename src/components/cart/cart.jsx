import React, { useState, useEffect } from "react";

import { destroy } from "../../redux/cartReducer";

import { motion, AnimatePresence } from "framer-motion"

import Item from "./item";
import { useSelector, useDispatch } from "react-redux";
import { getProducts } from "../../redux/productsReducer";
import { useNavigate } from "react-router-dom";
import { useGetProductsQuery } from "../../redux/apiSlice";


export default function Cart({ visible, closeHandler }) {
	const nav = useNavigate()

	const items = useSelector((state) => state.cart.items)

	const dispatch = useDispatch()

	const {data:products,isSuccess,
		isLoading,
		isFetching,
		isError,
		error} = useGetProductsQuery();

	return (<AnimatePresence>
		{
			visible &&
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1 }}
				exit={{ opacity: 0 }}
				transition={{ duration: .2 }}
				className="relative z-50"
				aria-labelledby="slide-over-title"
				role="dialog"
				aria-modal="true"
			>
				<div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
				<div className="fixed inset-0 overflow-hidden">
					<div className="absolute inset-0 overflow-hidden">
						<div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10">
							<div className="pointer-events-auto w-screen max-w-md">
								<div className="flex h-full flex-col overflow-y-scroll bg-white shadow-xl">
									<div className="flex-1 overflow-y-auto py-6 px-4 sm:px-6">
										<div className="flex items-start justify-between">
											<h2
												className="text-lg font-medium text-gray-900"
												id="slide-over-title"
											>
												Shopping cart
											</h2>
											<div className="ml-3 flex h-7 items-center">
												<button
													type="button"
													onClick={closeHandler}
													className="-m-2 p-2 text-gray-400 hover:text-gray-500"
												>
													<span className="sr-only">Close panel</span>
													<svg
														className="h-6 w-6"
														fill="none"
														viewBox="0 0 24 24"
														strokeWidth="1.5"
														stroke="currentColor"
														aria-hidden="true"
													>
														<path
															strokeLinecap="round"
															strokeLinejoin="round"
															d="M6 18L18 6M6 6l12 12"
														/>
													</svg>
												</button>
											</div>
										</div>
										<div className={`mt-8  ${items.length === 0 ? "h-[80%] flex justify-center items-center" : ""}`}>
											<div className="flow-root">
												{
													(items.length !== 0) ?

                                                        <ul role="list" className="-my-6 divide-y divide-gray-200">
                                                            {
                                                                items.map((e, index) => (
                                                                    <li key={index} className="flex py-6">
                                                                        <Item key={index} id={e.id} name={e.name} price={e.price} quantity={e.quantity} image={e.image} variant={e.variant} stock={150} handleRemove={() => { dispatch(destroy({ id: e.id, variant:e.variant })) }} />
                                                                    </li>

																))

															}

														</ul>

														:

														<div className=" flex justify-center items-center text-xl text-gray-600 italic">
															<h1>empty cart</h1>
														</div>
												}
											</div>
										</div>
									</div>
									<div className="border-t border-gray-200 py-6 px-4 sm:px-6">
										<div className="flex justify-between text-base font-medium text-gray-900">
											<p>Subtotal</p>
											<p>{items.reduce((total, e) => total + e.price * e.quantity, 0).toFixed(2)} DH</p>
										</div>
										<p className="mt-0.5 text-sm text-gray-500">
											Shipping and taxes calculated at checkout.
										</p>
										<div className="mt-6">
											<button
												onClick={() => nav("payment/step-1")}
												disabled={(items.length === 0) ? true : false}
												className={`flex items-center w-full justify-center rounded-md border border-transparent bg-secondary-300 px-6 py-3 text-base font-medium text-white shadow-sm ${(items.length !== 0) ? 'hover:bg-secondary-500' : ''}`}
											>
												Proceed
											</button>
										</div>
										<div className="mt-6 flex justify-center text-center text-sm text-gray-500">
											<p>
												or
												<button
													type="button"
													className="font-medium p-2 text-primary-500 hover:text-primary-900"
													onClick={closeHandler}
												>
													Continue Shopping
													<span aria-hidden="true"> →</span>
												</button>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</motion.div>
		}
	</AnimatePresence>
	)
}