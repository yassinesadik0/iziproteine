import React, { useEffect, useState } from "react";
import {useDispatch, useSelector} from "react-redux"
import { decrementQuantity, incrementQuantity, setQuantity } from "../../redux/cartReducer";


export default function Item({name, image, quantity, price, handleRemove,stock, id, variant}) {
    const dispatch = useDispatch()
    
    const [inputQuantity, setInputQuantity] = useState(quantity)

    const Validation = (e)=>{
        
        if (Number(e.value) > stock) {
            setInputQuantity(stock)
            dispatch(setQuantity({id:id,quantity:50, stock:stock, variant: variant }))
            return;
        }
        
        
        if (Number(e.value) < 1) {
            setInputQuantity(1)
            dispatch(setQuantity({id:id,quantity:1, stock:stock, variant: variant}))
            return;
        }
    
        if (isNaN(Number(e.value))) {
            setInputQuantity(quantity)
            return;
        }
        dispatch(setQuantity({id:id,quantity:Number(inputQuantity), stock:stock, variant: variant}))
    }

    useEffect(() => {
        console.log(stock)
    }, [stock])

        
    
    return (
        <>
            <div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                <img
                    src={image}
                    alt={name}
                    className="h-full w-full object-cover object-center"
                />
            </div>
            <div className="ml-4 flex flex-1 flex-col">
                <div>
                    <div className="flex justify-between text-base font-medium text-gray-900">
                        <h3>
                            <a href="#">{name}</a>
                        </h3>
                        <p className="ml-4">{price} DH</p>
                    </div>
                </div>
                        <p className="text-gray-600 pt-3">{variant}</p>
                <div className="flex flex-1 items-end justify-between text-sm">
                    <p className="text-gray-500">Qty <button onClick={()=>{dispatch(decrementQuantity({id:id, stock:stock}));setInputQuantity(inputQuantity>1?inputQuantity-1:inputQuantity)}} className="p-1" >-</button><input value={inputQuantity} onChange={e=>setInputQuantity(Number(e.target.value))} onBlur={e=>Validation(e.target)} className="w-1/5 border-none p-0 text-center" type="text" pattern="[0-9]+" /><button className="p-1" onClick={()=>{dispatch(incrementQuantity({id:id, stock:stock}));setInputQuantity(inputQuantity<stock?inputQuantity+1:inputQuantity)}}>+</button></p>
                    <div className="flex">
                        <button
                            type="button"
                            className="font-medium text-danger-600 hover:text-danger-800"
                            onClick={()=>{handleRemove()}}
                        >
                            Remove
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}