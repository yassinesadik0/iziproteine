import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { usePutOrderStatusMutation } from '../redux/apiSlice'
import { ToastContainer, toast } from 'react-toastify'
import Input from './Input'

function OrdersTable({ onRefetch, data }) {

	const [headers, setHeaders] = useState(["id", "client", "total", "status", "delivery"])

	const [orders, setOrders] = useState(data)

	const [filter, setFilter] = useState({
		status: '',
		delivery: ''
	})

	const [search, setSearch] = useState("")

	const onFilter = ()=>{
		console.log(filter);
		if(filter.status === '' && filter.delivery == ''){
			setOrders(data)
			console.log("found base", orders);
		}
		if(filter.status !== ''){
			setOrders(data.filter(e => e.status === filter.status))
			console.log("found status", data.filter(e => e.status === filter.status), orders);
		}
		if(filter.delivery !== ''){
			setOrders(data.filter(e => e.delivery === filter.delivery))
			console.log("found delivery", data.filter(e => e.delivery === filter.delivery), orders);
		}
		if(search !== ""){
			console.log("found number", data.filter(e =>e.client.phone.indexOf(search) !== -1));
			setOrders(data.filter(e => e.client.phone.indexOf(search) !== -1))
		}
	}

	const onChangeFilter = (e) => {
		setFilter({ ...filter, [e.target.name]: e.target.value === '' ? '' : e.target.value });
	}
	const [update, { isSuccesss, isLoading, isError, data: paload }] = usePutOrderStatusMutation()

	const onClickUpdate = (credentials) => {
		update(credentials)
			.then(() => {
				toast.success("updated successfully ! ");
				onRefetch();
			})
	}


	useEffect(() => {
		onFilter()
	}, [filter, search])



	return (
		<section className="container mx-auto w-full text-white lg:py-20  bg-success-600">

			<div className="px-10">
				<h1 className='text-center text-3xl font-bold text-white'>Search By Phone Number :</h1>
				<div className='flex flex-row justify-center items-center p-5 gap-5'>
					<Input onGetAmountValue={(e)=>{setSearch(e);console.log(e)}} value={search} fileClass={"bg-white mt-3"} placeholder={"phone number "} />
				</div>

			</div>
			<div className='w-full text-center m-5'>    <span className='font-bold'> Status </span>: <label htmlFor="">confirmed :<input onChange={onChangeFilter} value={"confirmed"} type="radio" className='form-radio text-warning-500' name="status" id="" /></label>  <label htmlFor="">not confirmed :<input onChange={onChangeFilter} value={"not confirmed"} type="radio" className='text-warning-500' name="status" id="" /></label> <label htmlFor=""> all : <input onChange={onChangeFilter} value={''} type="radio" defaultChecked name='status' className='text-warning-500' /></label> </div>
			<div className='w-full text-center m-5'>    <span className='font-bold'>Delivery</span>  : <label htmlFor="">delivered:<input onChange={onChangeFilter} value={"delivered"} type="radio" className='text-warning-500' name="delivery" id="" /></label>  <label htmlFor="">in progress :<input onChange={onChangeFilter} value={"in progress"} type="radio" className='text-warning-500' name="delivery" id="" /></label> <label htmlFor=""> all : <input onChange={onChangeFilter} value={''} type="radio" defaultChecked name='delivery' className='text-warning-500' /></label> <br /></div>

			<ToastContainer />
			<div className="flex flex-col ">
				<div className="overflow-x-auto ">
					<div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8 ">
						<div className="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg ">
							<table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700" >
								<thead className="bg-gray-50 dark:bg-gray-800">
									<tr>
										{
											headers.map((header, index) => {
												return <th
													key={index}
													className={`py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400`}
												>
													{header}
												</th>
											})
										}

										<th
											className={`py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400`}
										>
											Actions
										</th>
									</tr>
								</thead>
								<tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
									{
										orders.length < 1 ? <tr><td colSpan={6} className='text-gray-500 font-light text-center'> No Orders</td></tr> : orders.map((item, index) => {
											return (
												<tr key={index}>
													{
														headers.map((header, headIndex) => {

															return <td key={headIndex} className="px-4 py-4 text-sm font-medium text-gray-700 dark:text-gray-200 whitespace-nowrap">
																<div className="inline-flex items-center gap-x-3">
																	<span>{header != "client" ? item[header] : item[header].phone}</span>
																</div>
															</td>
														})
													}
													<td className="px-4 py-4 text-sm whitespace-nowrap">
														<div className="flex items-center gap-x-6">
															<Link
																to={`/admin/orders/${item.id}`}
																className="text-yellow-600 transition-colors duration-200 hover:text-primary-500 dark:text-gray-300 focus:outline-none">
																Show
															</Link>
															{
																item.status != "confirmed" ?
																	<>
																		<button
																			onClick={e => { onClickUpdate({ id: item.id, data: { status: "confirmed", delivery: "in progress" } }) }}
																			className="text-green-600 transition-colors duration-200 hover:text-primary-500 dark:text-gray-300 focus:outline-none">
																			Confirm
																		</button>
																		<button
																			onClick={e => { onClickUpdate({ id: item.id, data: { reject: "true" } }) }}
																			className="text-danger-600 transition-colors duration-200 hover:text-danger-700 dark:text-gray-300 focus:outline-none">
																			Reject
																		</button>
																	</> : (item.delivery == "delivered" ?
																		<></>
																		:
																		<>
																			<button
																				onClick={e => { onClickUpdate({ id: item.id, data: { status: "confirmed", delivery: "delivered" } }) }}
																				className="text-green-600 transition-colors duration-200 hover:text-primary-500 dark:text-gray-300 focus:outline-none">
																				Delivered
																			</button>
																			<button
																				onClick={e => { onClickUpdate({ id: item.id, data: { reject: "true" } }) }}
																				className="text-danger-600 transition-colors duration-200 hover:text-danger-700 dark:text-gray-300 focus:outline-none">
																				Returned
																			</button>
																		</>)

															}
														</div>
													</td>
												</tr>
											)
										})
									}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	)
}

export default OrdersTable