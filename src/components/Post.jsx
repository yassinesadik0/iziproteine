
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { motion } from 'framer-motion'

export default function Post({ data: { link, photo, createdAt, title, description } }) {
	const [zoom, setZoom] = useState(false)
	const item = {
		hidden: { y: 20, opacity: 0 },
		visible: {
			y: 0,
			opacity: 1
		}
	};

	return (
		<motion.a
			className='lg:h-1/2 w-fit h-fit lg:rounded-2xl overflow-hidden pb-8 lg:pb-0'
			onHoverStart={() => setZoom(true)}
			onHoverEnd={() => setZoom(false)}
			variants={item}
			target='_blank'
			href={link}
		>
			<div
				style={{ scale: 1, boxShadow: "rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px" }} className={`flex flex-col lg:flex-row  bg-cardBg rounded-lg gap-5 justify-center lg:items-center items-start w-fit h-full lg:rounded-2xl overflow-hidden`}>
				<div className="md:h-full w-full overflow-hidden lg:rounded-bl-lg rounded-tr-lg lg:rounded-tr-none rounded-tl-lg">
					<motion.img src={photo ? photo.file_path : "https://www.hostinger.com/tutorials/wp-content/uploads/sites/2/2021/09/how-to-write-a-blog-post.png"}
						animate={zoom ? { scale: 1.1 } : {}}
						transition={{ duration: 0.4, ease: "easeInOut", stiffness: 100 }}
						className="object-cover h-full w-full" alt="" />
				</div>

				<div className='h-full flex flex-col justify-center items-start gap-3 px-7'>
					<h5 className='font-normal text-primary-500 lg:text-sm text-xs '>{createdAt}</h5>
					<h3 className='font-bold text-secondary-600 lg:text-xl text-lg '>{title}</h3>
					<p className='font-medium text-secondary-500 lg:text-lg text-sm '>{description}</p>
				</div>
			</div>
		</motion.a>
	)
}
