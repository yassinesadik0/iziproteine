import Input from '../Input'
import Map from './Map'


function Contact({ section }) {

	return (
		<section id='contact' className='px-6 py-12  md:px-32 bg-transparent text-left relative h-fit lg:mt-20 flex flex-col xl:block justify-center gap-10 items-center'>
			<div className='md:h-[800px] flex items-center'>
				<form className="w-full max-w-lg h-fit">
					<div className="flex flex-wrap mb-6 gap-y-4">
						<h2 className='font-bold text-3xl md:text-4xl lg:text-5xl text-secondary-600'>Contactez <span className='text-primary-400'>nous</span></h2>
						<h4 className='font-semibold text-sm text-secondary-500'>N'hésitez pas à nous contacter dès maintenant pour nous faire part de vos questions, suggestions ou commentaires.</h4>
					</div>
					<div className="flex flex-wrap -mx-3 mb-6">
						<div className="w-full px-3">
							<label
								className="block uppercase tracking-wide text-secondary-500 text-xs font-bold mb-2"
								htmlFor="name"
							>
								Nom
							</label>
							<Input type={"text"} name={"name"} placeholder={"Entrer votre nom..."} />
							<p className="text-gray-600 text-xs italic">

							</p>
						</div>
					</div>
					<div className="flex flex-wrap -mx-3 mb-6">
						<div className="w-full px-3">
							<label
								className="block uppercase tracking-wide text-secondary-500 text-xs font-bold mb-2"
								htmlFor="email"
							>
								E-mail
							</label>
							<Input type={"email"} name={"email"} placeholder={"Entrer votre email..."} />
							<p className="text-gray-600 text-xs italic">

							</p>
						</div>
					</div>
					<div className="flex flex-wrap -mx-3 mb-6">
						<div className="w-full px-3">
							<label
								className="block uppercase tracking-wide text-secondary-500 text-xs font-bold mb-2"
								htmlFor="message"
							>
								Message
							</label>
							<textarea
								className=" resize-none appearance-none block w-full bg-gray-200 text-secondary-500 border border-slate-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 h-48 text-sm"
								id="message"
								defaultValue={""}
								placeholder='Entrer votre message...'
							/>
							<p className="text-gray-600 text-xs italic">

							</p>
						</div>
					</div>
					<div className="">
						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="button"
						>
							Envoyer
						</button>
					</div>
					<div className="md:w-2/3" />
				</form>
			</div>
			<div className='xl:w-1/3 xl:bg-secondary-400 w-full h-[400px] xl:h-full lg:w-1/2 xl:absolute md:block xl:right-0 xl:top-0 '>
				<div className='h-full w-full xl:w-[380px] xl:h-[500px] xl:right-56 xl:top-20: 2xl:w-[500px] 2xl:h-[650px] z-10 xl:absolute 2xl:right-96 xl:top-36'>
					<Map road={"Rue+Oued+Ziz"} city={"Agadir+80000"} country={"Morocco"} />
				</div>

			</div>
		</section>
	)
}

export default Contact
