
import Post from '../Post'
import { useEffect, useState } from "react"
import { motion } from 'framer-motion'
import { useGetPostsQuery } from '../../redux/apiSlice'



export default function Posts() {
	const {
		data: posts,
		isSuccess,
		isLoading,
	} = useGetPostsQuery();

	const [zoomm, setZoomm] = useState(false)
	const container = {
		hidden: { opacity: 1, y: 10 },
		visible: {
			opacity: 1,
			y: 0,
			transition: {
				delayChildren: 0.3,
				staggerChildren: 0.2
			}
		}
	};


	return (
		<section id="blog" className='px-6 py-12 md:px-32 bg-transparent text-left h-fit lg:mt-20'>
			<h2 className="text-center mx-auto font-bold text-3xl lg:text-4xl text-secondary-800 mb-20">Nos dernières <span className="text-gradient to-right">nouvelles</span> </h2>
			<section className="grid lg:grid-cols-2 grid-cols-1 gap-12 items-stretch justify-items-center">
				{
					posts ? <motion.div
						onHoverStart={() => setZoomm(true)}
						onHoverEnd={() => setZoomm(false)}
						variants={container}
						initial="hidden"
						whileInView="visible"
					>
						<a
							target='_blank'
							href={posts[0]?.Link}
							style={{ scale: 1, boxShadow: "rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px" }}
							className={`flex flex-col xlg:flex-row pb-8 h-fit lg:h-full bg-cardBg rounded-lg gap-5 justify-start lg:items-center items-start lg:rounded-2xl overflow-hidden`}>
							<div className="md:h-full w-full overflow-hidden">
								<motion.img src={posts[0].photo ? posts[0].photo.file_path :"https://www.hostinger.com/tutorials/wp-content/uploads/sites/2/2021/09/how-to-write-a-blog-post.png"}
									animate={zoomm ? { scale: 1.1 } : {}}
									transition={{ duration: 0.4, ease: "easeInOut", stiffness: 100 }}
									className="object-cover h-full w-full" alt="" />

							</div>
							<div className='h-full flex flex-col justify-center items-start gap-3 px-7'>
								<h5 className='font-normal text-primary-500 text-xs lg:text-sm'>{posts[0]?.createdAt}</h5>
								<h3 className='font-bold text-secondary-600 text-lg lg:text-xl'>{posts[0]?.title}</h3>
								<p className='font-medium text-secondary-500 text-sm lg:text-lg '>{posts[0]?.description}</p>
							</div>
						</a>
					</motion.div> : "loading..."
				}
				<motion.div
					className='flex flex-col gap-12'
					variants={container}
					initial="hidden"
					whileInView="visible"
				>
					{
						posts && Array.from(posts).map((post, index) => {
							if (index !== 0 && index < 3) {
								return <Post key={index} data={post} />
							}
						})
					}
				</motion.div>
			</section>

		</section>
	)
}