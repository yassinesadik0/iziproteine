import img from "../../assets/icons/arrow-cta.svg"
import { ReactCompareSlider, ReactCompareSliderImage } from 'react-compare-slider';
import waste from "../../assets/hero/waste.png"
import green from "../../assets/hero/green.png"
import logo1 from "../../assets/logo-bar/ci.png"
import logo2 from "../../assets/logo-bar/moubadara.png"
import logo3 from "../../assets/logo-bar/forsa.png"
import logo4 from "../../assets/logo-bar/lala.png"
import logo5 from "../../assets/logo-bar/bidaya1.png"
import logo6 from "../../assets/logo-bar/cfc1.png"
import logo7 from "../../assets/logo-bar/cise1.png"
import logo8 from "../../assets/logo-bar/orange1.png"
import logo9 from "../../assets/logo-bar/orm1.png"
import logo10 from "../../assets/logo-bar/val1.png"
import { motion } from "framer-motion"
import CustomButtonMotion from "../CustomButtonMotion"
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { A11y, Autoplay, Navigation, Pagination } from "swiper";


const photos = [
	logo1,
	logo2,
	logo3,
	logo4,
	logo5,
	logo6,
	logo7,
	logo8,
	logo9,
	logo10,
	
]

function Hero() {

	const textVariants = {
		initial: { opacity: 0, y: 50 },
		view: { opacity: 1, y: 0 },
	};

	return (
		<div className="flex justify-center items-center md:px-32  bg-transparent text-center lg:text-left xl:h-max ">
			<div>
				<main className=" container mx-auto">
					<div className=" flex flex-col lg:justify-evenly lg:flex-row gap-y-8 lg:h-hero md:items-center">
						<div className="flex flex-col items-center lg:items-start gap-y-4 lg:w-txtWidth mt-12 lg:mt-0 xl:w-1/2">
							<motion.h1 variants={textVariants} initial="initial" whileInView="view" transition={{ duration: 1, ease: "easeInOut" }} className="text-3xl xl:text-5xl md:text-4xl lg:w-full font-bold tracking-tight text-secondary-800">	Une alternative <motion.span
								className="text-gradient to-right">écologique</motion.span> et
								durable</motion.h1>
							<motion.h4 variants={textVariants} initial="initial" whileInView="view" transition={{ duration: 1, ease: "easeInOut", delay: 0.5 }} className="md:font-medium text-sm text-secondary-500 md:text-lg">Solutions durables de gestion des déchets organique <br /> Bioconversion par l'utilisation d'insectes.</motion.h4>
							<CustomButtonMotion
								text={"Découvrez-nous"}
								to={"/products"}
								img={img}
								classNames={"flex justify-center items-center w-fit h-8 px-8 py-5 md:w-52 md:h-14 md:mt-8 md:px-7 md:py-3  font-semibold text-white md:font-bold text-xs md:text-sm lg:text-md md:rounded-xl rounded-lg transition duration-150 ease-in-out cursor-pointer"}
							/>
						</div>
						<motion.div
							className="xl:w-[600px] lg:w-heroImg rounded-3xl overflow-hidden"
							whileInView={{ opacity: 1 }}
							initial={{ opacity: 0 }}
							transition={{ ease: "easeInOut", duration: 0.5 }}
						>
							<ReactCompareSlider
								changePositionOnHover
								boundsPadding={50}
								itemOne={<ReactCompareSliderImage src={green} srcSet={green} alt="Image one" />}
								itemTwo={<ReactCompareSliderImage src={waste} srcSet={waste} alt="Image two" />}
							/>
						</motion.div>
					</div>
				</main>
				<div className="container hidden lg:block my-[5%] w-full">
					<Swiper
						modules={[Navigation, Pagination, Autoplay, A11y]}
						slidesPerView={4}
						spaceBetween={5}
						className="flex justify-center items-center"
						autoplay={{
							delay: 500,
							disableOnInteraction: false,
						}}
						loop={true}
						speed={1000}

					>
						{
							photos.map((e, i) => (
								<SwiperSlide className="flex justify-center items-center"
									style={{
										width: "100px",
									}}
								>
									<img src={e} alt="" className="h-8 md:h-12 lg:h-[100px] object-cover" />
								</SwiperSlide>
							))
						}
					</Swiper>
				</div>
			</div>

		</div>
	);
}
// test

export default Hero;