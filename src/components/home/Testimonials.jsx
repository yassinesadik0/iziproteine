import React from 'react'
import { Navigation, EffectCreative, Pagination } from 'swiper'
import { SwiperSlide, Swiper } from 'swiper/react'
import 'swiper/css';

const testimonials = [
	{
		image: "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/24/a1/0a/10/entree-principale.jpg?w=1200&h=-1&s=1",
		description: "Nous sommes extrêmement satisfaits de notre achat en grande quantité chez vous. La qualité exceptionnelle de votre produit a dépassé nos attentes et a eu un impact significatif sur notre entreprise.",
		name: "Crocoparc",
		position: "Entreprise"

	},
]



function Testimonials() {

	return (
		<section className="px-6 py-12 md:px-32  bg-transparent md:text-left lg:mt-20">
			<h1 className='w-full mt-20 text-center mx-auto font-bold text-3xl lg:text-4xl  text-secondary-800 mb-1'>L'avis de nos <span className="text-gradient to-right">clients</span> </h1>
			{/* ====== Testimonials Section Start */}
			<div className="container mx-auto">
				<div>
					<div className="relative flex justify-center">
						<div className="relative w-full pb-16 md:w-11/12 lg:w-10/12 xl:w-8/12 xl:pb-0">
							<div
								className="snap xs:max-w-[368px] flex-no-wrap mx-auto flex h-auto w-full max-w-[300px] overflow-hidden transition-all sm:max-w-[508px] md:max-w-[630px] lg:max-w-[738px] 2xl:max-w-[850px]"
								x-ref="carousel"
							>
								<Swiper
									// slidesPerView={1}
									spaceBetween={30}
									effect={"creative"}
									creativeEffect={{
										prev: {
											shadow: true,
											translate: [0, 0, -400],
										},
										next: {
											translate: ["100%", 0, 0],
										},
									}}
									// navigation={true}
									pagination={{
										clickable: true,
									}}
									modules={[EffectCreative, Navigation, Pagination]}
									className="w-600 flex justify-center h-fit z-40"
								>
									{
										testimonials.map((e, index) =>
											<SwiperSlide
												key={index}
												className='p-5 sm:p-24'
											>
												<div className="w-full items-center md:flex">
													<div className="relative mb-12 w-full max-w-[310px] md:mr-12 md:mb-0 md:max-w-[250px] lg:mr-14 lg:max-w-[280px] 2xl:mr-16">
														<img
															src={e.image}
															alt="image"
															className="w-full"
														/>
														<span className="absolute -top-6 -left-6 z-[-1] hidden sm:block">
														</span>
														<span className="absolute -bottom-6 -right-6 z-[-1]">
															<svg
																width={64}
																height={64}
																viewBox="0 0 64 64"
																fill="none"
																xmlns="http://www.w3.org/2000/svg"
															>
																<path
																	d="M3 32C3 15.9837 15.9837 3 32 3C48.0163 2.99999 61 15.9837 61 32C61 48.0163 48.0163 61 32 61C15.9837 61 3 48.0163 3 32Z"
																	stroke="#13C296"
																	strokeWidth={6}
																/>
															</svg>
														</span>
													</div>
													<div className="w-full">
														<div>
	
															<p className="text-body-color mb-11 text-xs font-medium italic sm:text-lg">
																{e.description}
															</p>
															<h4 className="text-dark text-xl font-semibold">
																{e.name}
															</h4>
															<p className="text-body-color text-base">
																{e.position}
															</p>
														</div>
													</div>
												</div></SwiperSlide>

										)
									}
								</Swiper>
							</div>
						</div>
					</div>
				</div>
			</div>
			{/* ====== Testimonials Section End */}

		</section>
	)
}

export default Testimonials
