import Feature from "../Feature";
import { motion } from "framer-motion";
import img1 from "../../assets/features/Featuredicon-1.svg"
import img2 from "../../assets/features/Featuredicon-2.svg"
import img3 from "../../assets/features/Featuredicon-3.svg"
import img4 from "../../assets/features/Featuredicon-4.svg"
import img5 from "../../assets/features/Featuredicon-5.svg"
import img6 from "../../assets/features/Featuredicon.svg"

let features = [
	{
		img: img4,
		heading: "Solutions adaptées",
		paragraph: "Des solutions fiables et adaptées au contexte marocain et africain pour répondre à un problème croissant de déchets organiques."
	},
	{
		img: img6,
		heading: "Durabilité",
		paragraph: "Traiter les déchets organiques de manière durable tout en limitant les impacts négatifs sur l'environnement et en contribuant à des systèmes agroalimentaires plus durables."
	},
	{
		img: img1,
		heading: "Économie circulaire",
		paragraph: "S'inscrire dans une approche industrielle de l'économie circulaire pour valoriser les déchets alimentaires et développer les circuits courts, ainsi qu'une meilleure gestion des ressources."
	},
	{
		img: img2,
		heading: "Impact environnemental",
		paragraph: "Contribuer à bâtir des villes durables et résilientes qui fonctionnent comme de véritables écosystèmes, dans lesquels des aliments sains et nutritifs sont produits tout en luttant contre le gaspillage alimentaire et les changements climatiques."
	},
	{
		img: img3,
		heading: "Innovation",
		paragraph: "Un projet innovant de traitement de biodéchets alimentaires avec une phase industrielle visant à offrir une meilleure qualité de vie et un environnement plus sain pour les populations locales."
	},
	{
		img: img5,
		heading: "Nos Produits",
		paragraph: "Nous élevons des insectes de manière durable pour produire des produits biologiques de haute qualité, sans OGM ni additifs, au Maroc. Nous effectuons des contrôles rigoureux pour garantir la qualité et la sécurité de nos produits, avec une traçabilité totale."
	},
]

function Features() {

	const container = {
		hidden: { opacity: 1, scale: 0 },
		visible: {
			opacity: 1,
			scale: 1,
			transition: {
				delayChildren: 0.3,
				staggerChildren: 0.2
			}
		}
	};

	const item = {
		hidden: { y: 20, opacity: 0 },
		visible: {
			y: 0,
			opacity: 1
		}
	};

	return (
		<section className="px-6 py-12 md:px-32  bg-transparent md:text-left lg:mt-20">
			<motion.div className="grid xl:grid-cols-3 lg:grid-cols-2 items-start "
				variants={container}
				initial="hidden"
				whileInView="visible"
			>
				{
					features.map((feature, index) => { return <motion.div key={index} variants={item}><Feature data={feature} /></motion.div> })
				}
			</motion.div>
		</section>
	);
}

export default Features;