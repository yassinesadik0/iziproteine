import Stat from "../Stat";
import { useGetPublicStatisticsQuery } from "../../redux/apiSlice";


function Stats() {

	const {
		data: publicStatistics,
		isLoading,
		isSuccess,
		isError
	} = useGetPublicStatisticsQuery();

	let stats = [
		{
			number: publicStatistics?.wasteTreated,
			operator: "KG",
			paragraph: "Déchets traités"
		},
		{
			number: publicStatistics?.co2Reduced,
			operator: "KG",
			paragraph: "Réduction de CO2"
		},
		{
			number: publicStatistics?.energySaved,
			operator: "KWh",
			paragraph: "Économies d'énergie"
		},
	]

	return (
		<section className=" px-6 py-12 md:px-32  bg-transparent md:text-left lg:mt-20 ">
			<h2 className="text-center mx-auto font-bold text-3xl lg:text-4xl text-secondary-800 mb-20">Nos <span className="text-gradient to-right">résultats</span> </h2>
			<div className="grid lg:grid-cols-3 grid-cols-2 gap-x-12 gap-y-8 justify-items-center items-center ">
				{
					stats.map((stat, index) => <Stat key={index} data={stat} />)
				}

			</div>
		</section>

	);
}


export default Stats;