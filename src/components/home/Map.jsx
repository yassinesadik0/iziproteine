import React from 'react'

function Map({ road, city, country }) {
    
    return (
        <iframe
            className={`w-full h-full`}
            loading="lazy"
            allowFullScreen
            referrerPolicy="no-referrer-when-downgrade"
            src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyBn400jiCm91fhq681giZVXXnAkx2jCQ5Q&q=${road},+${city},+${country}`}
        ></iframe>
    )
}

export default Map
