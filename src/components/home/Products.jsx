import React from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import { motion } from "framer-motion";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import Product from "../Product";
import { useGetProductsQuery } from "../../redux/apiSlice";

function Products() {

	const container = {
		hidden: { opacity: 1, scale: 0 },
		visible: {
			opacity: 1,
			scale: 1,
			transition: {
				delayChildren: 0.3,
				staggerChildren: 0.2
			}
		}
	};

	const item = {
		hidden: { y: 20, opacity: 0 },
		visible: {
			y: 0,
			opacity: 1
		}
	};

	const {
		data: products,
		isSuccess,
		isLoading
	} = useGetProductsQuery();

	return (
		<div id="products" className="px-6 py-12 md:px-32 bg-transparent text-left lg:mt-20 h-fit">
			<h2 className="text-center mx-auto font-bold text-3xl lg:text-4xl  text-secondary-800 mb-20">Nos <span className="text-gradient to-right">produits</span></h2>
			<div className="md:hidden block">
					<Swiper
						slidesPerView={2}
						spaceBetween={30}
						centeredSlides={true}
					>
						{
							isSuccess && products.map((product, index) => <SwiperSlide className="rounded-lg" key={index}><Product key={index} data={product} /></SwiperSlide>)
						}
					</Swiper>
			</div>
			<motion.div
				className="hidden md:grid 2xl:grid-cols-5 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 gap-x-12 gap-y-8 justify-items-center"
				variants={container}
				initial="hidden"
				whileInView="visible"
			>
				{
					isLoading ? "loading" : ""
				}
				{
					isSuccess ? products.map((product, index) => <motion.div key={index} variants={item}><Product data={product} /></motion.div>) : "loading"
				}
			</motion.div>
		</div>
	);
}

export default Products;