import cartImg from "../assets/products/addCart.svg"
import cartImg_disabled from "../assets/products/addCart-disabled.svg"
import { motion } from "framer-motion";
import { useDispatch } from "react-redux"
import { add } from "../redux/cartReducer";
import { useEffect, useState } from "react";
import Bag from "../assets/products/bag.png"
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";


function Product({ data }) {
	const dispatch = useDispatch()
	const [variant, setVariant] = useState(0);
	const [price, setPrice] = useState(data.variants[0].price);

	return (
		<motion.div
			style={{ boxShadow: "rgba(17, 17, 26, 0.1) 0px 4px 16px, rgba(17, 17, 26, 0.1) 0px 8px 24px, rgba(17, 17, 26, 0.1) 0px 16px 56px" }}
			className="my-[50px] flex flex-col  rounded-lg gap-5 justify-center items-center xl:h-full   xl:px-8 xl:py-6 lg:px-6 lg:py-6 px-5 py-4 md:h-max md:w-max md:rounded-2xl"
		>
			<div className={`overflow-hidden`}>
				<Swiper
					slidesPerView={1}
					spaceBetween={30}
					centeredSlides={true}
					className="h-[150px] w-[150px]"
				>
					{
						data?.photos?.map((photo, index) => <SwiperSlide className="rounded-lg" key={index}><img src={photo?.file_path} className="object-cover" alt="product image" /></SwiperSlide>)
					}

				</Swiper>
				{/* <img src={Bag} className="w-full object-cover" alt="product image" /> */}
			</div>
			<div className="flex flex-row justify-center items-start gap-4 w-max">
				<div className="flex flex-col justify-center items-start gap-3">
					<h4 className="font-medium text-[9px] text-secondary-500 md:text-sm">{data.name}</h4>
					<select onChange={e => { setPrice(data.variants[e.currentTarget.value].price); setVariant(e.currentTarget.value) }} className="bg-transparent border-2 border-gray-400 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-4/5 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
						{
							data.variants.map((e, index) => <option key={index} value={index}>{e.weight >= 1000 ? `${e.weight / 1000}kg` : `${e.weight}g`}</option>)
						}
					</select>
					<h3 className="font-bold text-sm text-secondary-600 md:text-lg">{price} DH</h3>
					{
						data.stock !== 0 ? <h4 className="font-normal text-[9px] md:text-sm text-secondary-500 text-left">{data.stock}<span> Produit en stock</span></h4> : <h4 className="font-normal text-[9px] md:text-sm text-danger-500 text-left">Rupture de stock</h4>
					}
				</div>
				{
					data.stock !== 0 ?
						<button onClick={() => dispatch(add({ id: data.id, name: data.name, variant: data.variants[variant].weight >= 1000 ? `${data.variants[variant].weight / 1000}kg` : `${data.variants[variant].weight}g`, image: data.photos[0].file_path, price: price, variant_id: data.variants[variant].id }))}>
							<img className="w-8 md:w-[42px] " src={cartImg} alt="" />
						</button> :
						<div>
							<img className="w-8 md:w-[42px] " src={cartImg_disabled} alt="" />
						</div>

				}

			</div>
		</motion.div>
	)
}

export default Product;