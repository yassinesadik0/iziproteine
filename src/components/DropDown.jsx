import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import icon from "../assets/icons/icon.png";

function DropDown({ role }) {
	const [toggle, setToggle] = useState(false)
	const [logoutResponse, setLogoutResponse] = useState(null)
	const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")))

	const handleLogOut = (e) => {
	
		localStorage.removeItem("authToken")
		localStorage.removeItem("user")
		window.location.reload();


	}

	// useEffect(() => {
	window.addEventListener("click", (e) => {
		if (e.target.id === "account") {
			return;
		}
		setToggle(false)
	})
	// }, [])



	return (
		<div className="flex justify-center">
			<div>
				<div className="relative" data-te-dropdown-ref>
					<img
						id="account"
						onClick={() => setToggle(!toggle)}
						className="shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-outhover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] motion-reduce:transition-none w-12 rounded-full"
						data-te-dropdown-toggle-ref
						aria-expanded="false"
						data-te-ripple-init
						data-te-ripple-color="light"
						src={user?.photo ? user.photo : icon}
						alt="Avatar" />
					{
						toggle ? role.toLowerCase() === "admin" ? <ul
							className="absolute z-[1000] float-left m-0 min-w-max list-none overflow-hidden rounded-lg border-none bg-white bg-clip-padding text-left text-base shadow-lg dark:bg-neutral-700"
							aria-labelledby="dropdownMenuButton1"
							data-te-dropdown-menu-ref
						>
							<li>
								<Link
									className="block w-full whitespace-nowrap bg-transparent py-2 px-4 text-sm font-normal text-neutral-700 hover:bg-neutral-100 active:text-neutral-800 active:no-underline disabled:pointer-events-none disabled:bg-transparent disabled:text-neutral-400 dark:text-neutral-200 dark:hover:bg-neutral-600"
									to="/admin"
									data-te-dropdown-item-ref=""
								>
									Dashboard
								</Link>
							</li>
							<li>
								<button
									className={
										"block w-full whitespace-nowrap bg-transparent py-2 px-4 text-sm font-normal text-neutral-700 hover:bg-neutral-100 active:text-neutral-800 active:no-underline disabled:pointer-events-none disabled:bg-transparent disabled:text-neutral-400 dark:text-neutral-200 dark:hover:bg-neutral-600"
									}
									type="button"
									onClick={handleLogOut}
								>
									logout
								</button>
							</li>
						</ul> : <ul
							className="absolute z-[1000] float-left m-0 min-w-max list-none overflow-hidden rounded-lg border-none bg-white bg-clip-padding text-left text-base shadow-lg dark:bg-neutral-700"
							aria-labelledby="dropdownMenuButton1"
							data-te-dropdown-menu-ref
						>
							<li>
								<Link
									className="block w-full whitespace-nowrap bg-transparent py-2 px-4 text-sm font-normal text-neutral-700 hover:bg-neutral-100 active:text-neutral-800 active:no-underline disabled:pointer-events-none disabled:bg-transparent disabled:text-neutral-400 dark:text-neutral-200 dark:hover:bg-neutral-600"
									to="/profile"
									data-te-dropdown-item-ref=""
								>
									Profile
								</Link>
							</li>
							<li>
								<button
									className={
										"block w-full whitespace-nowrap bg-transparent py-2 px-4 text-sm font-normal text-neutral-700 hover:bg-neutral-100 active:text-neutral-800 active:no-underline disabled:pointer-events-none disabled:bg-transparent disabled:text-neutral-400 dark:text-neutral-200 dark:hover:bg-neutral-600"
									}
									type="button"
									onClick={handleLogOut}
								>
									logout
								</button>
							</li>
						</ul> : ""
					}

				</div>
			</div>
		</div>

	)
}

export default DropDown
