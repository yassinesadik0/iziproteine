import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useGetPostsQuery } from '../../redux/apiSlice';

function SingleBlog() {
	const params = useParams()
	const {
		data: posts,
		isSuccess,
		isLoading,
	} = useGetPostsQuery();
	// tinymce.init({
	// 	selector: "#contentPara",
	// });
	let post = posts?.find(post => post.id == params.id)

	return (
		<>
			{/*Title*/}
			<div className="text-center pt-16 md:pt-32">
				<p className="text-sm md:text-base text-green-500 font-bold">
					{post?.createdAt}
				</p>
				<h1 className="font-bold break-normal text-3xl md:text-5xl">
					{post?.title}
				</h1>
			</div>
			{/*image*/}
			<div
				className="container w-full max-w-6xl mx-auto bg-white bg-cover mt-8 rounded"
				style={{
					backgroundImage: `url(${post?.photo?.file_path})`,
					height: "75vh"
				}}
			/>
			{/*Container*/}
			<div className="container max-w-5xl mx-auto -mt-32">
				<div className="mx-0 sm:mx-6">
					<div
						className="bg-white w-full p-8 md:p-24 text-xl md:text-2xl text-gray-800 leading-normal"
					>
						<p dangerouslySetInnerHTML={{ __html: post?.content }} className="text-2xl md:text-3xl mb-5">
						</p>
					</div>
				</div>
			</div>
		</>

	)
}

export default SingleBlog
