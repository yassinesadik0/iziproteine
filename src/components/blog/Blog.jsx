import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useGetPostsQuery } from '../../redux/apiSlice'



function Blog() {
	const {
		data: posts,
		isSuccess,
		isLoading,
		error,
	} = useGetPostsQuery();


	useEffect(() => {
		console.log(posts, error, isSuccess, isLoading)
	}, [posts, isSuccess, isLoading])


	return (
		<>
			{/*Container*/}
			<div className="container px-4 md:px-0 max-w-6xl mx-auto mt-32">
				<div className="mx-0 sm:mx-6">

					<div className="w-full text-xl md:text-2xl text-gray-800 leading-normal rounded-t">

						{/*Posts Container*/}
						<div className="flex flex-wrap justify-between pt-12 -mx-6">
							{/*1/3 col */}

							{isLoading?<h1>Loading...</h1>
							:
								posts ? Array.from(posts).map((post, index) => {
									console.log(post)
									if (index < 3) return (
										<div key={index} className="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
											<div className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
												<a
													href={post?.link}
													target='_blank'
													className="flex flex-wrap no-underline hover:no-underline"
												>
													<img
														src={post.photo ? post.photo.file_path : "https://www.hostinger.com/tutorials/wp-content/uploads/sites/2/2021/09/how-to-write-a-blog-post.png"}
														className="h-64 w-full object-cover rounded-t pb-6"
													/>
													<p className="w-full text-gray-600 text-xs md:text-sm px-6">
														{post?.createdAt}
													</p>
													<div className="w-full font-bold text-xl text-gray-900 px-6">
														{post?.title}
													</div>
													<p className="text-gray-800  text-base px-6 mb-5">
														{post?.description}
													</p>
												</a>
											</div>

										</div>
									)
								}) : undefined
							}


							{
								posts ? Array.from(posts).map((post, index) => {
									if (index > 2 && index < 6) return (
										<div key={index} className="w-full md:w-1/2 p-6 flex flex-col flex-grow flex-shrink">
											<div className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
												<a
													href={post?.link}
													target='_blank'
													className="flex flex-wrap no-underline hover:no-underline"
												>
													<img
														src={post.photo ? post.photo.file_path : "https://www.hostinger.com/tutorials/wp-content/uploads/sites/2/2021/09/how-to-write-a-blog-post.png"}
														className="h-full w-full rounded-t pb-6"
													/>
													<p className="w-full text-gray-600 text-xs md:text-sm px-6">
														{post?.createdAt}
													</p>
													<div className="w-full font-bold text-xl text-gray-900 px-6">
														{post?.title}
													</div>
													<p className="text-gray-800  text-base px-6 mb-5">
														{post?.description}
													</p>
												</a>
											</div>
										</div>
									)


								}) : undefined
							}
						</div>
						{/*/ Post Content*/}
					</div>

				</div>
			</div>
		</>
	)
}

export default Blog
