import CountUp from 'react-countup';
import { motion } from 'framer-motion';
import { useState } from 'react';

function Stat({ data: { number, operator, paragraph } }) {
	const [inView, seInView] = useState(false)
	return (
		<motion.div
			className="lg:w-56 text-center"
			whileInView={() => { seInView(true) }}
		>
			<h3 className="font-bold text-2xl lg:text-5xl text-secondary-800"><CountUp end={number} duration={2} redraw={inView} /><span className="text-primary-500">{operator}</span></h3>
			<p className="font-norma text-sm lg:text-xl text-secondary-600">{paragraph}</p>
		</motion.div>
	);
}

export default Stat;