import React, { useState, useEffect, useRef } from 'react'
import Input from '../Input'
import { useGetRolesQuery } from '../../redux/apiSlice';
import axios from 'axios';
import { ToastContainer, toast } from "react-toastify";
import { BASE_URL } from '../../../config';


function CreateUser({ onRefetch, edit, id }) {
	const [user, setUser] = useState(null)
	const [errors, setErrors] = useState(null)
	const firstName = useRef(null)
	const lastName = useRef(null)
	const email = useRef(null)
	const telephone = useRef(null)
	const address = useRef(null)
	const password = useRef(null)
	const role = useRef(null)
	const photo = useRef(null)

	const {
		data: roles,
		isSuccess,
		isLoading,
		isFetching,
		isError,
		error,
	} = useGetRolesQuery();

	useEffect(() => {
		if (edit) {
			axios.get(BASE_URL+`users/${id}`)
				.then(response => setUser(response.data))
				.catch(err => console.error(err))
		}
	}, [id])

	const handleSubmit = (event) => {
		event.preventDefault()

		const myUser = {
			first_name: firstName.current.value,
			last_name: lastName.current.value,
			email: email.current.value,
			telephone: telephone.current.value,
			address: address.current.value,
			password: password.current.value,
			photo: photo.current.files[0],
			role_id: Number(role.current.value)
		}

		const myEditedUser = {
			_method: 'PUT',
			first_name: firstName.current.value,
			last_name: lastName.current.value,
			email: email.current.value,
			telephone: telephone.current.value,
			address: address.current.value,
			password: password.current.value,
			photo: photo.current.files[0],
			role_id: Number(role.current.value)
		}

		if (edit) {
			const myEditedUserConfig = {
				headers: {
					'Content-Type': `multipart/form-data; boundary=${myEditedUser._boundary}`,
					'Authorization': `Bearer ${localStorage.getItem("authToken")}`
				}
			};

			axios.post(BASE_URL+`users/${id}`, myEditedUser, myEditedUserConfig)
				.then(response => {
					firstName.current.value = ""
					lastName.current.value = ""
					email.current.value = ""
					password.current.value = ""
					address.current.value = ""
					telephone.current.value = ""
					role.current.value = null
				})
				.catch(error => {
					console.error(error);
					setErrors(error.response.data.errors);
				});
		} else {


			const myUserConfig = {
				headers: {
					'Content-Type': `multipart/form-data; boundary=${myUser._boundary}`,
					'Authorization': `Bearer ${localStorage.getItem("authToken")}`
				}
			};

			axios.post(BASE_URL+'register', myUser, myUserConfig)
				.then(response => {
					localStorage.setItem("authToken", response.data.authToken)
					firstName.current.value = ""
					lastName.current.value = ""
					email.current.value = ""
					password.current.value = ""
					address.current.value = ""
					telephone.current.value = ""
					role.current.value = null
					toast.success("The user was created successfully")
					onRefetch()
				})
				.catch(error => {
					console.error(error);
					setErrors(error.response.data.errors);
					toast.error("There was an error while creating the user")
				});
		}
	}

	return (
		<>
			{/* Row */}
			<div className="w-full lg:w-7/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto">
				<ToastContainer />
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">
					{
						edit ? "Edit a User" : "Create a User"
					}
				</h3>
				<form onSubmit={handleSubmit} className="px-8 pt-6 pb-8 mb-4  rounded">
					<div className="mb-4 md:flex md:justify-between">
						<div className="mb-4 md:w-1/2 md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="firstName"
							>
								First name
							</label>
							<Input
								type={"text"}
								name={"firstName"}
								c_ref={firstName}
								placeholder={"First name"}
								error={!!errors?.first_name}
							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.first_name : undefined
								}
							</p>
						</div>
						<div className="md:ml-2 md:w-1/2">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="lastName"
							>
								Last name
							</label>
							<Input
								name="lastName"
								type="text"
								placeholder="Last name"
								c_ref={lastName}
								error={!!errors?.last_name}
							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.last_name : undefined
								}
							</p>
						</div>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="email"
						>
							Email
						</label>
						<Input
							name="email"
							type="email"
							placeholder="Email"
							c_ref={email}
							error={!!errors?.email}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.email : undefined
							}
						</p>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="password"
						>
							Password
						</label>
						<Input
							name="password"
							type="password"
							placeholder="Password"
							c_ref={password}
							error={!!errors?.password}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.password : undefined
							}
						</p>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="tel"
						>
							Telephone
						</label>
						<Input
							name="tel"
							type="tel"
							placeholder="Telephone"
							c_ref={telephone}
							error={!!errors?.telephone}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.telephone : undefined
							}
						</p>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="image"
						>
							Image
						</label>
						<Input
							name="image"
							type="file"
							placeholder="Image"
							c_ref={photo}
						/>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="author"
						>
							Role
						</label>

						{
							isSuccess && roles && edit && user ? <select
								className={`${errors?.role_id ? "border-2 border-danger-600" : "border border-gray-400"} appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm`}
								name="user"
								ref={role}
							>
								<option value={undefined} aria-readonly>Select role</option>
								{
									roles.map((role, index) => {
										return (
											<option key={index} selected={role.role === user.role} value={role.id}>{role.role}</option>
										)
									})
								}
							</select> : <select
								className={`${errors?.role_id ? "border-2 border-danger-600" : "border border-gray-400"} appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm`}
								name="author"
								ref={role}
							>
								<option value={undefined} aria-readonly>Select role</option>
								{
									roles && roles.map((role, index) => {
										return (
											<option key={index} value={role.id}>{role.role}</option>
										)
									})
								}
							</select>
						}
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.role_id : undefined
							}
						</p>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="address"
						>
							Address
						</label>
						<Input
							name="address"
							type="text"
							placeholder="Address"
							c_ref={address}
							error={!!errors?.address}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.address : undefined
							}
						</p>
					</div>

					<div className="mb-6 text-center">
						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="submit"
						>
							{
								edit ? "Edit User" : "Create User"
							}
						</button>
					</div>
				</form>
			</div>
		</>

	)
}

export default CreateUser
