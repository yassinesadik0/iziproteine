import React, { useState, useEffect, useRef } from 'react'
import Input from '../Input'
import { useGetFilteredUsersQuery, useGetWasteTypesQuery } from '../../redux/apiSlice';
import axios from 'axios';
import { ToastContainer, toast } from "react-toastify";
import { BASE_URL } from '../../../config';


function CreateWaste({ edit, id }) {
	const [waste, setWaste] = useState(null)
	const [errors, setErrors] = useState(null)
	const quantity = useRef(null)
	const quality = useRef(null)
	const type = useRef(null)
	const supplier = useRef(null)
	const moderator = useRef(null)

	const {
		data: moderators,
		isSuccess,
		isLoading,

	} = useGetFilteredUsersQuery("Moderator");

	const {
		data: suppliers,
		isSuccess: success,
		isLoading: loading,

	} = useGetFilteredUsersQuery("Supplier");

	const {
		data: types,
		isSuccess: typeSuccess,
		isLoading: typeLoading,

	} = useGetWasteTypesQuery();

	useEffect(() => {
		if (edit) {
			axios.get(BASE_URL+`wastes/${id}`)
				.then(response => setWaste(response.data))
				.catch(err => console.error(err))
		}
	}, [id])

	const handleSubmit = (event) => {
		event.preventDefault()
		setErrors(null)
		const myWaste = {
			quantity: quantity.current.value,
			quality: quality.current.value,
			waste_types_id: type.current.value,
			supplier_id: supplier.current.value,
			moderator_id: moderator.current.value,
		}

		const myEditedWaste = {
			_method: 'PUT',
			quantity: quantity.current.value,
			quality: quality.current.value,
			waste_types_id: type.current.value,
			supplier_id: supplier.current.value,
			moderator_id: moderator.current.value,
		}

		if (edit) {
			const myEditedWasteConfig = {
				headers: {
					'Content-Type': `multipart/form-data; boundary=${myEditedWaste._boundary}`,
					'Authorization': `Bearer ${localStorage.getItem("authToken")}`
				}
			};

			axios.post(BASE_URL+`wastes/${id}`, myEditedWaste, myEditedWasteConfig)
				.then(response => {
					quantity.current.value = ""
					quality.current.value = ""
				})
				.catch(error => {
					setErrors(error.response.data.errors);
				});
		} else {


			const myWasteConfig = {
				headers: {
					'Content-Type': `multipart/form-data; boundary=${myWaste._boundary}`,
					'Authorization': `Bearer ${localStorage.getItem("authToken")}`
				}
			};

			axios.post(BASE_URL+'wastes', myWaste, myWasteConfig)
				.then(response => {
					quantity.current.value = ""
					quality.current.value = ""
					toast.success("The waste was created successfully")
				})
				.catch(error => {
					setErrors(error.response.data.errors);
					toast.error("There was an error creating the waste")
				});
		}
	}
	return (
		<>
			<div className="w-full lg:w-7/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto">
				<ToastContainer />
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">
					{
						edit ? "Edit a Record" : "Create a Record"
					}
				</h3>
				<form onSubmit={handleSubmit} className="px-8 pt-6 pb-8 mb-4  rounded">
					{
						edit ? <div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="id"
							>
								Waste ID
							</label>
							<Input
								type={"text"}
								name={"id"}
								value={waste ? waste.id : ""}
								disabled
							/>
						</div> : null
					}
					<div className="mb-4 md:flex md:justify-between">
						<div className="mb-4 md:w-1/2 md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="wasteType"
							>
								Waste type
							</label>
							{/* will be fetched from the wasteType table  */}
							{
								typeSuccess && edit && waste ? <select
									className="appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm"
									name="user"
									ref={type}
								>
									<option value={undefined} aria-readonly>Select type</option>
									{
										types.map((type, index) => {
											return (
												<option key={index} selected={type.type === waste.type} value={type.id}>{type.type}</option>
											)
										})
									}
								</select> : <select
									className="appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm"
									name="author"
									ref={type}
								>
									<option value={undefined} aria-readonly>Select Type</option>
									{
										types && types.map((type, index) => {
											return (
												<option key={index} value={type.id}>{type.type}</option>
											)
										})
									}
								</select>
							}
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.waste_types_id : undefined
								}
							</p>
						</div>
						<div className="md:ml-2 md:w-1/2">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="quantity"
							>
								Quantity
							</label>
							<Input
								name="quantity"
								type="number"
								placeholder="Quantity"
								c_ref={quantity}
								value={edit && waste ? waste.quantity : ""}
								error={!!errors?.quantity}
							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.quantity : undefined
								}
							</p>
						</div>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="supplier"
						>
							Supplier Name
						</label>
						{
							success && edit && waste ? <select
								className="appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm"
								name="user"
								ref={supplier}
							>
								<option value={undefined} aria-readonly>Select Supplier</option>
								{
									suppliers.map((supplier, index) => {
										return (
											<option key={index} selected={supplier.name === waste.supplier} value={supplier.id}>{supplier.name}</option>
										)
									})
								}
							</select> : <select
								className="appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm"
								name="author"
								ref={supplier}
							>
								<option value={undefined} aria-readonly>Select Supplier</option>
								{
									suppliers && suppliers.map((supplier, index) => {
										return (
											<option key={index} value={supplier.id}>{supplier.name}</option>
										)
									})
								}
							</select>
						}
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.supplier_id : undefined
							}
						</p>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="moderator"
						>
							Moderator Name
						</label>
						{
							isSuccess && edit && waste ? <select
								className="appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm"
								name="user"
								ref={moderator}
							>
								<option value={undefined} aria-readonly>Select Moderator</option>
								{
									moderators.map((moderator, index) => {
										return (
											<option key={index} selected={moderator.name === waste.moderator} value={moderator.id}>{moderator.name}</option>
										)
									})
								}
							</select> : <select
								className="appearance-none block w-full bg-gray-200 text-secondary-500 border border-gray-400 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm"
								name="author"
								ref={moderator}
							>
								<option value={undefined} aria-readonly>Select Moderator</option>
								{
									moderators && moderators.map((moderator, index) => {
										return (
											<option key={index} value={moderator.id}>{moderator.name}</option>
										)
									})
								}
							</select>
						}
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.moderator_id : undefined
							}
						</p>
					</div>

					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="quality"
						>
							Quality
						</label>
						<Input
							name="quality"
							type="number"
							placeholder="Quality"
							c_ref={quality}
							value={edit && waste ? waste.quality : ""}
							error={!!errors?.quality}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.quality : undefined
							}
						</p>
						<span className='text-sm font-bold text-primary-500'>The quality should be between 1 and 10 as of (2/10)</span>

					</div>

					<div className="mb-6 text-center">
						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="submit"
						>
							{
								edit ? "Edit Record" : "Create Record"
							}
						</button>
					</div>
				</form>
			</div>
		</>

	)
}

export default CreateWaste
