import React, { useEffect, useRef, useState } from 'react'
import Input from '../Input'
import { useGetFilteredUsersQuery } from '../../redux/apiSlice';
import axios from 'axios';
import { ToastContainer, toast } from "react-toastify";
import { BASE_URL } from '../../../config';

function CreateBlog({ edit, id }) {

	const title = useRef(null)
	const link = useRef(null)
	const [errors, setErrors] = useState(null)
	const [post, setPost] = useState(null)
	const photo = useRef(null)
	const description = useRef(null)
	
	const {
		data: authors,
		isSuccess,
		isLoading,
		isFetching,
		isError,
	} = useGetFilteredUsersQuery("Admin");

	useEffect(() => {
		if (edit) {
			axios.get(BASE_URL+`posts/${id}`)
				.then(response => {
					setPost(response.data)
				})
				.catch(err => console.error(err))
		}
	}, [id])

	const handleSubmit = (event) => {
		event.preventDefault()

		setErrors(null)
		const post = {
			title: title.current.value,
			photo: photo.current.files[0],
			link: link.current.value,
			description: description.current.value
		}
		const editedPost = {
			_method: 'PUT',
			title: title.current.value,
			photo: photo.current.files[0],
			link: link.current.value,
			description: description.current.value
		}

		if (edit) {
			const editedPostConfig = {
				headers: {
					'Content-Type': `multipart/form-data; boundary=${editedPost._boundary}`,
					'Authorization': `Bearer ${localStorage.getItem("authToken")}`

				}
			};

			axios.post(BASE_URL+`posts/${id}`, editedPost, editedPostConfig)
				.then(response => {
					title.current.value = ""
					link.current.value = ""
					description.current.value = ""
					toast.success("The Post was updated successfully")
				})
				.catch(error => {
					setErrors(error.response?.data.errors);
					toast.error("There was an error creating the post")
				});
		} else {


			const postConfig = {
				headers: {
					'Content-Type': `multipart/form-data; boundary=${post._boundary}`,
					'Authorization': `Bearer ${localStorage.getItem("authToken")}`
				}
			};

			axios.post(BASE_URL+'posts', post, postConfig)
				.then(response => {
					title.current.value = ""
					link.current.value = ""
					description.current.value = ""
					toast.success("The Post was created successfully")
				})
				.catch(error => {
					console.log(error)
					setErrors(error.response?.data.errors);
					toast.error("There was an error creating the post")

				});
		}


	}

	return (
		<>
			{/* Row */}
			<div className="w-full lg:w-10/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto">
				<ToastContainer />
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">
					{
						edit ? "Edit a Post" : "Create a Post"
					}
				</h3>
				<form onSubmit={handleSubmit} className="px-8 pt-6 pb-8 mb-4  rounded">
					{
						edit ? <div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="id"
							>
								Post ID
							</label>
							<Input
								type={"text"}
								name={"id"}
								value={post ? post.id : ""}
								disabled
							/>
						</div> : null
					}
					<div className="mb-4 md:flex md:justify-between">
						<div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="title"
							>
								Post title
							</label>
							<Input
								type={"text"}
								name={"title"}
								placeholder={"Post title"}
								c_ref={title}
								value={edit && post ? post.title : ""}
								error={!!errors?.title}
							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.title : undefined
								}
							</p>
						</div>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="photo"
						>
							Image
						</label>
						<Input
							name="photo"
							type="file"
							placeholder="Image"
							c_ref={photo}
							error={!!errors?.photo}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.photo : undefined
							}
						</p>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="description"
						>
							Link
						</label>
						<Input
							name="link"
							type="text"
							placeholder="Link (Lien)"
							c_ref={link}
							value={edit && post ? post.link : ""}
							error={!!errors?.link}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.link : undefined
							}
						</p>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="description"
						>
							Description
						</label>
						<Input
							name="description"
							type="text"
							placeholder="Description"
							c_ref={description}
							value={edit && post ? post.description : ""}
							error={!!errors?.description}
						/>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors ? errors.description : undefined
							}
						</p>
					</div>

					<div className="mb-6 text-center">
						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="submit"
						>
							{
								edit ? "Edit Post" : "Create Post"
							}
						</button>
					</div>
				</form>
			</div>
		</>

	)
}

export default CreateBlog