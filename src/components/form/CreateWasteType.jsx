import React, { useEffect, useRef, useState } from 'react'
import Input from '../Input'
import axios from 'axios';
import { ToastContainer, toast } from "react-toastify";


function CreateWasteType({ edit, id }) {
	const type = useRef(null)
	const [errors, setErrors] = useState(null)
	const [wasteType, setWasteType] = useState(null)


	useEffect(() => {
		if (edit) {
			axios.get(BASE_URL+`wasteTypes/${id}`)
				.then(response => setWasteType(response.data))
				.catch(err => console.error(err))
		}
	}, [id])

	const handleSubmit = (event) => {
		event.preventDefault()

		const myType = {
			type: type.current.value,

		}
		const myEditedType = {
			_method: 'PUT',
			type: type.current.value,
		}

		if (edit) {
			axios.post(BASE_URL+`wasteTypes/${id}`, myEditedType, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem("authToken")}`
				}
			})
				.then(response => {
					type.current.value = ""

				})
				.catch(error => {
					setErrors(error.response.data.errors);
				});

		} else {
			axios.post(BASE_URL+'wasteTypes', myType, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem("authToken")}`
				}
			})
				.then(response => {
					type.current.value = "" 
					toast.success("The waste type was created successfully")
				})
				.catch(error => {
					setErrors(error.response.data.errors);
					toast.error("There was an error creating the waste type")

				});
		}


	}

	return (
		<>
			{/* Row */}
			<div className="w-full lg:w-7/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto">
			<ToastContainer />
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">
					{
						edit ? "Edit a Waste Type" : "Create a Waste Type"
					}
				</h3>
				<form onSubmit={handleSubmit} className="px-8 pt-6 pb-8 mb-4  rounded">
					{
						edit ? <div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="id"
							>
								Waste Type ID
							</label>
							<Input
								type={"text"}
								name={"id"}
								value={wasteType ? wasteType.id : ""}
								disabled
							/>
						</div> : null
					}
					<div className="mb-4 md:flex md:justify-between">
						<div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="type"
							>
								Waste Type name
							</label>
							<Input
								type={"text"}
								name={"type"}
								placeholder={"Waste Type name"}
								c_ref={type}
								value={edit && wasteType ? wasteType.wasteType : ""}
								error={!!errors?.type}

							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.type : undefined
								}
							</p>
						</div>
					</div>
					<div className="mb-6 text-center">
						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="submit"
						>
							{
								edit ? "Edit Waste Type" : "Create Waste Type"
							}
						</button>
					</div>
				</form>
			</div>
		</>

	)
}

export default CreateWasteType