import React, { useRef, useState } from 'react'
import Input from '../Input'
import Select from 'react-select';
import makeAnimated from 'react-select/animated';


const animatedComponents = makeAnimated();
const colorOptions = [
	{ value: 'red', label: 'gg' },
	{ value: 'green', label: 'Green' },
	{ value: 'blue', label: 'Blue' },
	{ value: 'purple', label: 'Purple' },
	{ value: 'orange', label: 'Orange' },
	{ value: 'yellow', label: 'Yellow' },
]

function CreateDiscount() {
	const [minAmount, setMinAmount] = useState(null)
	const selectRef = useRef(null)

	function clearSelect() {
		selectRef.current.clearValue()
	}

	function getAmountValue(min) {
		setMinAmount(min)
	}
	return (
		<>
			{/* Row */}
			<div className="w-full lg:w-7/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto">
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">Create a discount</h3>
				<form className="px-8 pt-6 pb-8 mb-4  rounded">
					<div className="mb-4 md:flex md:justify-between">
						<div className="mb-4 md:w-1/2 md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="discountName"
							>
								Discount Name
							</label>
							<Input
								name="discountName"
								type="text"
								placeholder="Discount Name"
							/>
						</div>
						<div className="md:ml-2 md:w-1/2">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="amount"
							>
								Amount to reduce in (%)
							</label>
							<Input
								name="amount"
								type="number"
								placeholder="Amount"
							/>
						</div>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="minPurchacedAmount"
						>
							Mininimum Purchaced Amount in (Units)
						</label>
						<Input
							onChange={clearSelect}
							onGetAmountValue={getAmountValue}
							name="minPurchacedAmount"
							type="number"
							placeholder="Min Purchaced Amount"
						/>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="startDate"
						>
							Start Date
						</label>
						<Input
							name="startDate"
							type="date"
							placeholder="Start Date"
						/>
					</div>

					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="endDate"
						>
							End Date
						</label>
						<Input
							name="endDate"
							type="date"
							placeholder="End Date"
						/>
					</div>
					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="endDate"
						>
							Products
						</label>


						<Select
							ref={selectRef}
							isDisabled={minAmount > 1}
							// clearValue={() => }
							closeMenuOnSelect={false}
							components={animatedComponents}
							isMulti
							options={colorOptions}
						/>

					</div>

					<div className="mb-6 text-center">
						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="button"
							onClick={() => clearSelect()}
						>
							Create a discount
						</button>
					</div>
				</form>
			</div>
		</>

	)
}

export default CreateDiscount
