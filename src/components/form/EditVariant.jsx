import React, { useEffect, useRef, useState } from 'react'
import Input from '../Input'
import axios from 'axios'


function EditVariant({ id }) {

	const [variant, setVariant] = useState(undefined)
	const [errors, setErrors] = useState(null)
	const price = useRef(null)
	const weight = useRef(null)

	useEffect(() => {
		axios.get(BASE_URL+`productVariants/${id}`)
			.then(response => setVariant(response.data))
			.catch(err => console.error(err))
	}, [id])

	const handleEdit = (e) => {
		e.preventDefault()
		let createdVariant = {
			id: id,
			product_id: variant.product_id,
			weight: Number(weight.current.value),
			price: Number(price.current.value),
		}
		axios.put(BASE_URL+`productVariants/${id}`, createdVariant, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("authToken")}`
			}
		})
			.then(response => {
				price.current.value = 0
				weight.current.value = 0
			})
			.catch(error => setErrors(error.response.data.errors))
	}

	return (
		<>
			{/* Row */}
			<div className="w-full lg:w-7/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto">
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">Edit a Variant</h3>
				{

					variant ? <form onSubmit={handleEdit} className="px-8 pt-6 pb-8 mb-4  rounded">
						<div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="id"
							>
								Variant ID
							</label>
							<Input
								type={"text"}
								name={"id"}
								value={id}
								disabled
							/>

						</div>
						<div className="mb-4 md:flex md:justify-between">

							<div className="mb-4 md:w-1/2 md:mr-2 md:mb-0">
								<label
									className="block mb-2 text-sm font-bold text-gray-500"
									htmlFor={`weight`}
								>
									Weight
								</label>
								<Input
									type={"number"}
									name={`weight`}
									value={variant.weight}
									c_ref={weight}
									placeholder={"Weight"}
									error={!!errors?.weight}

								/>
								<p className="block mb-2 text-sm font-semibold text-danger-600">
									{
										errors ? errors.weight : undefined
									}
								</p>
							</div>
							<div className="md:ml-2 md:w-1/2">
								<label
									className="block mb-2 text-sm font-bold text-gray-500"
									htmlFor={`price`}
								>
									Price
								</label>
								<Input
									name={`price`}
									value={variant.price}
									type="number"
									c_ref={price}
									placeholder="Price"
									error={!!errors?.price}

								/>
								<p className="block mb-2 text-sm font-semibold text-danger-600">
									{
										errors ? errors.price : undefined
									}
								</p>
							</div>
						</div>
						<div className="mb-6 text-center">
							<button
								className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
								type="submit"

							>
								Edit Variant
							</button>
						</div>
					</form> : "loading..."
				}
			</div>
		</>

	)
}

export default EditVariant
