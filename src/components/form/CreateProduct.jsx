import React, { useEffect, useRef, useState } from 'react'
import Input from '../Input'
import axios from 'axios'
import Bag from "../../assets/products/bag.png";
import { ToastContainer, toast } from "react-toastify";
import { BASE_URL } from '../../../config';

import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";

function CreateProduct({ create, edit, id }) {
	const [errors, setErrors] = useState(null)
	const [images, setImages] = useState([1])
	const [product, setProduct] = useState(null)
	const [variants, setVariants] = useState([{ weight: "", price: "" }])
	const [imageInputs, setImageInputs] = useState([])
	const name = useRef(null)
	const quantity = useRef(null)
	const [description, setDescription] = useState("")

	function handleFileInputs(e) {
		setImageInputs([...imageInputs, e.target.files[0]])
	}

	useEffect(() => {
		let fileInput = document.getElementsByClassName("file")
		Array.from(fileInput).forEach(input => input.addEventListener("change", handleFileInputs))

		return () => {
			Array.from(fileInput).forEach(input => input.removeEventListener("change", handleFileInputs))
		}
	}, [images.length])


	useEffect(() => {
		if (edit) {
			axios.get(BASE_URL+`products/${id}`)
				.then(response => setProduct(response.data))
				.catch(err => console.error(err))
		}
	}, [id])


	const addImageInput = () => {
		if (images.length >= 7) {
			return
		}
		setImages([...images, 1])
	}

	const addVariantInputs = () => {
		setVariants([...variants, { weight: "", price: "" }])
	}

	const handleSubmit = (event) => {
		event.preventDefault()
		let product = {
			name: name.current.value,
			stock: quantity.current.value,
			variants: variants,
			photos: imageInputs,
			description: description
		}
		let editedProduct = {
			_method: 'PUT',
			name: name.current.value,
			stock: quantity.current.value,
			photos: imageInputs,
			description: description
		}
		console.log(editedProduct)

		const config = {
			headers: {
				'Content-Type': `multipart/form-data; boundary=${product._boundary}`,
				'Authorization': `Bearer ${localStorage.getItem("authToken")}`
			}
		};

		if (edit) {
			axios.post(BASE_URL+`products/${id}`, editedProduct, {
				headers: {
					'Content-Type': `multipart/form-data; boundary=${editedProduct._boundary}`,
					'Authorization': `Bearer ${localStorage.getItem("authToken")}`
				}
			})
				.then(response => {
					console.log(editedProduct)
					name.current.value = ""
					quantity.current.value = ""
					setDescription("")
					toast.success("The Product was updated successfully")
				})
				.catch(error => {
					setErrors(error.response.data.errors);
					toast.error("There was an error updating the product")
				});
		} else {
			axios.post(BASE_URL+'products', product, config)
				.then(response => {
					name.current.value = ""
					quantity.current.value = ""
					setDescription("")
					toast.success("The Product was created successfully")
				})
				.catch(error => {
					setErrors(error.response.data.errors);
					toast.error("There was an error creating the product")
				});
		}


	}

	return (
		<>
			{/* Row */}
			<div className={create ? "w-full lg:w-7/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto" : ""}>
				<ToastContainer />
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">
					{
						edit ? "Edit a Product" : "Create a Product"
					}
				</h3>
				<form onSubmit={handleSubmit} method='post' className="px-8 pt-6 pb-8 mb-4  rounded">
					{
						edit ? <div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="name"
							>
								Product ID
							</label>
							<Input
								type={"text"}
								name={"name"}
								value={id}
								disabled
							/>
						</div> : null
					}

					<div className="mb-4 md:flex md:justify-between">
						<div className="mb-4 md:w-1/2 md:mr-2 md:mb-0">

							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="name"
							>
								Product Name
							</label>
							<Input
								type={"text"}
								name={"name"}
								placeholder={"Product name"}
								value={edit ? product?.name : undefined}
								c_ref={name}
								error={!!errors?.name}
							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.name : undefined
								}
							</p>
						</div>
						<div className="md:ml-2 md:w-1/2">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="stock"
							>
								Quantity
							</label>
							<Input
								name="stock"
								type="number"
								placeholder="Quantity"
								c_ref={quantity}
								value={edit ? product?.stock : undefined}
								error={!!errors?.stock}

							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.stock : undefined
								}
							</p>
						</div>
					</div>
					<div className={`${edit ? "" : "p-5 border border-secondary-400 rounded-lg mb-5"}`}>
						{
							!edit ? variants.map((variant, index) => {

								return (
									<div key={index}>
										<label
											className="block mb-2 text-lg font-bold text-gray-700"
											htmlFor=""
										>
											Variant {index + 1}
										</label>
										<div key={index} className="mb-4 md:flex md:justify-between">
											<div className="mb-4 md:w-1/2 md:mr-2 md:mb-0">
												<label
													className="block mb-2 text-sm font-bold text-gray-500"
													htmlFor={`weight ${index}`}
												>
													Weight
												</label>
												<Input
													type={"number"}
													name={`weight ${index}`}
													placeholder={"Weight"}
													onBlur={(e) => setVariants(prev => prev.map((item, index1) => index === index1 ? { ...item, weight: Number(e.target.value) } : item

													))}
													error={!!errors?.weight}
												/>
												<p className="block mb-2 text-sm font-semibold text-danger-600">
													{
														errors ? errors.weight : undefined
													}
												</p>
											</div>
											<div className="md:ml-2 md:w-1/2">
												<label
													className="block mb-2 text-sm font-bold text-gray-500"
													htmlFor={`price ${index}`}
												>
													Price
												</label>
												<Input
													name={`price ${index}`}
													type="number"
													placeholder="Price"
													onBlur={(e) => setVariants(prev => prev.map((item, index1) => index === index1 ? { ...item, price: Number(e.target.value) } : item
													))}
													error={!!errors?.price}

												/>
												<p className="block mb-2 text-sm font-semibold text-danger-600">
													{
														errors ? errors.price : undefined
													}
												</p>
											</div>
										</div>
									</div>
								)
							}) : ""
						}
					</div>
					{
						edit ? <Swiper
							modules={[Navigation]}
							spaceBetween={2}
							slidesPerView={1}
							speed={500}
							loop={true}
							navigation={true}
							className="w-600 flex justify-center h-fit z-40 bg-transparent "
							style={{ backgroundColor: "transparent" }}
						>
							{product?.photos.map((e, index) => <SwiperSlide style={{ backgroundColor: "transparent" }} className="rounded-lg " key={index}>
								<img key={index} className="w-2/4 mx-auto" src={e.file_path} alt="" />
							</SwiperSlide>)}
						</Swiper> : ""
					}
					{
						edit ? undefined : <button
							type='button'
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-fit mb-4 ml-0 px-4 rounded"
							onClick={addVariantInputs}
						>
							Add Variant
						</button>
					}
					<label
						className="block mb-2 text-sm font-bold text-gray-700"
						htmlFor="photos[]"
					>
						Images
					</label>

					{
						images.map((image, index) => {
							return (
								<div key={index} className="mb-4">
									<Input
										name="photos[]"
										type="file"
										placeholder="Image"
										fileClass="file"
										error={!!errors?.photos}
									/>
								</div>
							)
						})
					}
					<p className="block mb-2 text-sm font-semibold text-danger-600">
						{
							errors ? errors.photos : undefined
						}
					</p>

					<button
						type='button'
						className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-fit mb-4 ml-0 px-4 rounded"
						onClick={addImageInput}
					>Add Image
					</button>


					<div className="mb-4">
						<label
							className="block mb-2 text-sm font-bold text-gray-700"
							htmlFor="description"
						>
							Description
						</label>
						<textarea
							className={`appearance-none block w-full bg-gray-200 text-secondary-500 border ${errors?.description ? "border-2 border-danger-600" : "border border-gray-400"}  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm resize-none`}
							id="description"
							name="description"
							onChange={(e) => setDescription(e.target.value)}
							rows="5"
							// defaultValue={edit ? product?.description : undefined}
							value={description}
							placeholder="Description">
						</textarea>
						<p className="block mb-2 text-sm font-semibold text-danger-600">
							{
								errors?.description
							}
						</p>
					</div>

					<div className="mb-6 text-center">

						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="submit"
						>
							{
								edit ? "Edit Product" : "Create Product"
							}
						</button>
					</div>
				</form>
			</div>
		</>

	)
}

export default CreateProduct
