import React, { useEffect, useRef, useState } from 'react'
import Input from '../Input'
import axios from 'axios';
import { ToastContainer, toast } from "react-toastify";


function CreateRole({ edit, id }) {
	const roleName = useRef(null)
	const [role, setRole] = useState(null)
	const [errors, setErrors] = useState(null)



	useEffect(() => {
		if (edit) {
			axios.get(`http://127.0.0.1:8000/api/v1/roles/${id}`)
				.then(response => setRole(response.data))
				.catch(err => console.error(err))
		}
	}, [id])

	const handleSubmit = (event) => {
		event.preventDefault()
		setErrors(null)
		const myRole = {
			role: roleName.current.value,

		}
		const editedRole = {
			_method: 'PUT',
			role: roleName.current.value,
		}

		if (edit) {
			axios.post(`http://127.0.0.1:8000/api/v1/roles/${id}`, editedRole, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem("authToken")}`
				}
			})
				.then(response => {
					roleName.current.value = ""

				})
				.catch(error => {
					setErrors(error.response.data.errors);

				});

		} else {
			axios.post('http://127.0.0.1:8000/api/v1/roles', myRole, {
				headers: {
					Authorization: `Bearer ${localStorage.getItem("authToken")}`
				}
			})
				.then(response => {
					roleName.current.value = ""
					toast.success("The Role was created successfully")
				})
				.catch(error => {
					setErrors(error.response.data.errors);
					toast.error("There was an error creating the role")

				});
		}


	}

	return (
		<>
			{/* Row */}
			<div className="w-full lg:w-7/12 bg-bgGray p-5 rounded-lg lg:rounded-l-none mx-auto">
				<ToastContainer />
				<h3 className="pt-4 text-xl md:text-2xl text-center font-bold text-secondary-700">
					{
						edit ? "Edit a Role" : "Create a Role"
					}
				</h3>
				<form onSubmit={handleSubmit} className="px-8 pt-6 pb-8 mb-4  rounded">
					{
						edit ? <div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="id"
							>
								Role ID
							</label>
							<Input
								type={"text"}
								name={"id"}
								value={role ? role.id : ""}
								disabled
							/>
						</div> : null
					}
					<div className="mb-4 md:flex md:justify-between">
						<div className="mb-4 md:w-full md:mr-2 md:mb-0">
							<label
								className="block mb-2 text-sm font-bold text-gray-700"
								htmlFor="role"
							>
								Role name
							</label>
							<Input
								type={"text"}
								name={"role"}
								placeholder={"Role name"}
								c_ref={roleName}
								value={edit && role ? role.role : ""}
								error={!!errors?.role}

							/>
							<p className="block mb-2 text-sm font-semibold text-danger-600">
								{
									errors ? errors.role : undefined
								}
							</p>
						</div>
					</div>
					<div className="mb-6 text-center">
						<button
							className="shadow bg-gradient-to-r from-secondary-400 to-success-500 hover:bg-secondary-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 w-full px-4 rounded"
							type="submit"
						>
							{
								edit ? "Edit Role" : "Create Role"
							}
						</button>
					</div>
				</form>
			</div>
		</>

	)
}

export default CreateRole