import React, { useEffect, useRef, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios';
import { BASE_URL } from '../../config';



function Table({ onRefetch, data, products, execluded, table, uri }) {
	const [headers, setHeaders] = useState([])
	const [id, setId] = useState(-1)
	const [showModal, setShowModal] = useState(false);

	const handleModal = (item) => {
		setId(item["id"])
		setShowModal(true)
	}

	const handleDeletion = () => {
		axios.delete(BASE_URL+`${table}/${id}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("authToken")}`,
			},
		})
		.then(res => console.log(res)).catch(err => console.error(err))
		.then(() => onRefetch())
		setShowModal(false)
	}

	useEffect(() => {
		try {
			if (data.length !== 0) {
				setHeaders(Object.keys(data[0]).filter(key => !execluded.includes(key)))
			}
		} catch (err) {
			console.log(err.message)
		}
	}, [data.length])

	return (
		<section className="container mx-auto md:w-full  lg:py-20 border bg-success-600">
			<div className="flex flex-col ">
				<div className="overflow-x-auto ">
					<div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8 ">
						<div className="overflow-hidden border border-gray-200 dark:border-gray-700 md:rounded-lg ">
							<table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700" >
								<thead className="bg-gray-50 dark:bg-gray-800">
									<tr>
										{
											headers.map((header, index) => {
												return <th
													key={index}
													className={`py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400`}
												>
													{header}
												</th>
											})
										}

										<th
											className={`py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400`}
										>
											Actions
										</th>
									</tr>
								</thead>
								<tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
									{
										data ? Array.from(data).map((item, index) => {
											return (
												<tr key={index}>
													{
														headers.map((header, headIndex) => {
															return <td key={headIndex} className="px-4 py-4 text-sm font-medium text-gray-700 dark:text-gray-200 whitespace-nowrap">
																<div className="inline-flex items-center gap-x-3">
																	{
																		products && header === "variants" ? (<span>
																			{item[header].map((variant, index) => <div key={index}>{variant.weight}G<span className='ml-8 font-semibold text-success-800'>{variant.price} </span>DH<br /> <hr className='w-full h-[2px] bg-slate-500' /></div>)}
																		</span>) : <span>{item[header]}</span>
																	}
																</div>
															</td>
														})
													}

													<td className="px-4 py-4 text-sm whitespace-nowrap">
														<div className="flex items-center gap-x-6">
															{
																table === "users" ? undefined : <Link
																	to={`/admin/${uri}/${item["id"]}/edit`}
																	className="text-yellow-600 transition-colors duration-200 hover:text-primary-500 dark:text-gray-300 focus:outline-none"
																>
																	Edit
																</Link>
															}

															<Link
																// to={"/"}
																onClick={() => handleModal(item)}
																className="text-danger-600 transition-colors duration-200 hover:text-danger-700 dark:text-gray-300 focus:outline-none">
																Delete
															</Link>
														</div>
													</td>
												</tr>
											)
										}) : undefined
									}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			{
				showModal ? (
					<>
						<div
							className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
						>
							<div className="relative w-auto my-6 mx-auto max-w-3xl">
								{/*content*/}
								<div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
									{/*header*/}
									<div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
										<h2 className="block mb-2 text-lg font-bold text-gray-800">
											Confirm deletion
										</h2>
										<button
											className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
											onClick={() => setShowModal(false)}
										>
											<span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
												×
											</span>
										</button>
									</div>
									{/*body*/}
									<div className="relative p-6 flex-auto">
										<p className='mb-2 text-sm font-bold text-gray-700'>
											Are you sure you want to delete the {uri} with id: {id}
										</p>
									</div>
									{/*footer*/}
									<div className="flex items-center justify-center p-6 border-t border-solid border-slate-200 rounded-b">
										<button
											className="text-gray-600  font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
											type="button"
											onClick={() => setShowModal(false)}
										>
											Close
										</button>
										<button
											className="text-danger-600  font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
											type="button"
											onClick={() => handleDeletion()}
										>
											Delete
										</button>

									</div>
								</div>
							</div>
						</div>
						<div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
					</>
				) : null
			}
		</section>

	)
}

export default Table
