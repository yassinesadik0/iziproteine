import React from 'react'

function Input({ type, name, placeholder, error, c_ref, onGetAmountValue, onChange, onBlur, fileClass, disabled, value, id_value,onClick, ...rest }) {

	function handleChange(e) {
		onGetAmountValue(e.target.value)
		onChange(e)
	}

	return (
		<input
			className={`${error ? "border-2 border-danger-600" : "border border-gray-400"} appearance-none block w-full bg-transparent text-secondary-500   rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 text-sm  ${fileClass}`}
			id={name}
			name={name}
			type={type}
			placeholder={placeholder ? placeholder : ""}
			onChange={onGetAmountValue ? handleChange : null}
			onBlur={onBlur}
			onClick={onClick ?? undefined}
			ref={c_ref ?? null}
			disabled={disabled}
			defaultValue={value ?? undefined}
			// value={id_value ?? undefined}
			{...rest}
		/>
	)
}

export default Input
