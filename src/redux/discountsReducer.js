import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getDiscounts = createAsyncThunk("data/getDiscounts", async (thunkAPI) => {
	try {
		let response = await axios.get(
			"http://localhost:5173/src/redux/discounts.json"
		);
		let data = await response.data;
		return data;
	} catch (err) {
		return err.message;
	}
});

const discountsSlice = createSlice({
	name: "discounts",
	initialState: {
		discounts: [],
		status: "idle",
		error: null,
	},
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(getDiscounts.pending, (state) => {
				state.status = "loading";
			})
			.addCase(getDiscounts.fulfilled, (state, action) => {
				state.status = "succeeded";
				state.discounts = action.payload;
			})
			.addCase(getDiscounts.rejected, (state, action) => {
				state.status = "failed";
				state.error = action.error.message;
			});
	},
});

export default discountsSlice.reducer;
