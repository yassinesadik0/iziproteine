import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getSuppliers = createAsyncThunk("data/getSuppliers", async (thunkAPI) => {
	try {
		let response = await axios.get(
			"http://localhost:5173/src/redux/suppliers.json"
		);
		let data = await response.data;
		return data;
	} catch (err) {
		return err.message;
	}
});

const suppliersSlice = createSlice({
	name: "suppliers",
	initialState: {
		suppliers: [],
		status: "idle",
		error: null,
	},
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(getSuppliers.pending, (state) => {
				state.status = "loading";
			})
			.addCase(getSuppliers.fulfilled, (state, action) => {
				state.status = "succeeded";
				state.suppliers = action.payload;
			})
			.addCase(getSuppliers.rejected, (state, action) => {
				state.status = "failed";
				state.error = action.error.message;
			});
	},
});

export default suppliersSlice.reducer;
