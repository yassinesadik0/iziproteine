import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_URL } from "../../config";

export const apiSlice = createApi({
	reducerPath: "api",
	baseQuery: fetchBaseQuery({ baseUrl: BASE_URL }),

	endpoints: (builder) => ({
		putOrderStatus: builder.mutation({
			query: (credentials) => ({
				url: `/orders/${credentials.id}`,
				method: "PUT",
				headers: {
					Authorization:
						"Bearer " + localStorage.getItem("authToken"),
				},
				body: credentials.data,
			}),
		}),

		postOrder: builder.mutation({
			query: (credentials) => ({
				url: `/orders`,
				method: "POST",
				headers: {
					Authorization:
						"Bearer " + localStorage.getItem("authToken"),
					accept: "application/json",
				},
				body: credentials,
			}),
		}),

		postLogin: builder.mutation({
			query: (credentials) => ({
				url: `/login`,
				method: "POST",
				body: credentials,
			}),
		}),
		postToken: builder.mutation({
			query: (token) => ({
				url: `/checkToken`,
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			}),
		}),
		postLogOut: builder.mutation({
			query: (token) => ({
				url: `/logout`,
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			}),
		}),
		getProducts: builder.query({
			query: (token) => ({
				url: "/products",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			}),
		}),
		getVariants: builder.query({
			query: () => "/productVariants",
		}),
		getPublicStatistics: builder.query({
			query: () => "/publicWasteStatistic",
		}),
		getOrders: builder.query({
			query: (token) => ({
				url: "/orders",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			}),
		}),
		getPosts: builder.query({
			query: () => "/posts",
		}),
		getUsers: builder.query({
			query: () => "/users",
		}),
		getRoles: builder.query({
			query: () => "/roles",
		}),
		getWasteTypes: builder.query({
			query: () => "/wasteTypes",
		}),
		getDiscounts: builder.query({
			query: () => "/discounts.json",
		}),
		getFilteredUsers: builder.query({
			query: (query) => `/searchUsers?role=${query}`,
		}),
		getWaste: builder.query({
			query: () => "/wastes",
		}),
		postProduct: builder.mutation({
			query: (product) => ({
				url: `/products`,
				method: "POST",
				headers: {
					"Content-Type": `multipart/form-data; boundary=${product._boundary}`,
				},
				body: product,
			}),
		}),
		getProduct: builder.query({
			query: (productId) => `/products/${productId}`,
		}),
	}),
});

export const {
	useGetProductsQuery,
	useGetOrdersQuery,
	useGetPostsQuery,
	useGetUsersQuery,
	useGetWasteQuery,
	useGetDiscountsQuery,
	useGetVariantsQuery,
	useGetFilteredUsersQuery,
	useGetRolesQuery,
	useGetWasteTypesQuery,
	useGetPublicStatisticsQuery,
	usePostLogOutMutation,
	usePostLoginMutation,
	usePostTokenMutation,
	usePostProductMutation,
	usePutOrderStatusMutation,
	usePostOrderMutation,
} = apiSlice;
