import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getWaste = createAsyncThunk("data/getWaste", async (thunkAPI) => {
	try {
		let response = await axios.get(
			"http://localhost:5173/src/redux/waste.json"
		);
		let data = await response.data;
		return data;
	} catch (err) {
		return err.message;
	}
});

const wasteSlice = createSlice({
	name: "waste",
	initialState: {
		waste: [],
		status: "idle",
		error: null,
	},
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(getWaste.pending, (state) => {
				state.status = "loading";
			})
			.addCase(getWaste.fulfilled, (state, action) => {
				state.status = "succeeded";
				state.waste = action.payload;
			})
			.addCase(getWaste.rejected, (state, action) => {
				state.status = "failed";
				state.error = action.error.message;
			});
	},
});

export default wasteSlice.reducer;
