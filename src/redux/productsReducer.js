import {createSlice} from "@reduxjs/toolkit"
import {createAsyncThunk} from "@reduxjs/toolkit"
import axios from "axios"


export const getProducts = createAsyncThunk("data/getProducts" , async (thunkApi)=>{
    try {
        const response = await axios.get("http://localhost:5173/src/redux/products.json");
        const data = response.data;
        return data
    } catch (err) {
        return err.message
    }
})



const productsSlice = createSlice({
    name:"products",
    initialState:{
        products:[],
        status:"idle",
    },
    reducers:{
		fetch (state) {
			state.products = [...state.products, getProducts()]
		}
    },
    extraReducers:(build)=>{
        build
                .addCase(getProducts.pending, (state)=>{
                    state.status ="loading";
                    
                })
                .addCase(getProducts.fulfilled, (state, action)=>{
                    state.status ="succeeded";
                    state.products = action.payload
                })
                .addCase(getProducts.rejected, (state,action)=>{
                    state.status = "failed";
                    
                })
    }
})



export const { fetch } = productsSlice.actions;
export default productsSlice.reducer;

