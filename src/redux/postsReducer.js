import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const myPosts = createAsyncThunk("data/getPosts", async (thunkAPI) => {
	try {
		let response = await axios.get(
			"http://localhost:5173/src/redux/posts.json"
		);
		let data = await response.data;
		return data;
	} catch (err) {
		return err.message;
	}
});

const postsSlice = createSlice({
	name: "posts",
	initialState: {
		posts: [],
		status: "idle",
		error: null,
	},
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(myPosts.pending, (state) => {
				state.status = "loading";
			})
			.addCase(myPosts.fulfilled, (state, action) => {
				state.status = "succeeded";
				state.posts = action.payload;
			})
			.addCase(myPosts.rejected, (state, action) => {
				state.status = "failed";
				state.error = action.error.message;
			});
	},
});

export default postsSlice.reducer;
