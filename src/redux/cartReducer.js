import { createSlice } from "@reduxjs/toolkit"
import dummyProduct from "../assets/products/dummy-product.png"
import { getProducts } from "./productsReducer"


let initialState;

if (localStorage.getItem("cart")) {
    initialState = {
        items: JSON.parse(localStorage.getItem("cart")),

    }

} else {
    initialState = {
        items: [],
        total: 0,
    }

}

initialState = {...initialState, shipping_form: {
    first_name: null,
    last_name: null,
    email: null,
    phone: null,
    zip: null,
    city: null,
    address: null
}}



function isEven(n) {
    return n % 2 == 0;
}



const cartSlice = createSlice(
    {
        name: "cart",
        initialState,
        reducers: {
            destroy: (state, action) => {
                state.items = state.items.filter(e=>action.payload.id !== e.id || action.payload.variant !== e.variant)
                localStorage.setItem("cart", JSON.stringify(state.items))
            },
            add: (state, action) => {
                if ((state.items.filter(e => (e.id === action.payload.id)&&(e.variant === action.payload.variant)).length > 0)) {
                    return;
                }
                state.items = [...state.items, { ...action.payload, quantity: 1 }]
                localStorage.setItem("cart", JSON.stringify(state.items))

            },
            incrementQuantity: (state, action) => {
                state.items = state.items.map((e, index) => {
                    if (e.id === action.payload.id && e.quantity < action.payload.stock) {
                        console.log("incremented")
                        return { ...e, quantity: e.quantity + 1 }
                    }
                    console.log("not incremented",e.id, action.payload.id, e.quantity , action.payload.stock)
                    return e
                })
                localStorage.setItem("cart", JSON.stringify(state.items))
            },
            decrementQuantity: (state, action) => {
                state.items = state.items.map((e, index) => {
                    if (e.id === action.payload.id && e.quantity > 1) {
                        return { ...e, quantity: e.quantity - 1 }
                    }
                    return e
                })
                localStorage.setItem("cart", JSON.stringify(state.items))
            },
            setQuantity: (state, action) => {
                state.items = state.items.map((e, index) => {
                    if (e.id === action.payload.id && e.variant === action.payload.variant && action.payload.quantity >= 1 && action.payload.quantity <= action.payload.stock && !isNaN(action.payload.quantity)) {
                        return { ...e, quantity: action.payload.quantity }
                    }
                    return e
                })
                localStorage.setItem("cart", JSON.stringify(state.items))
            },
            setTotal: (state, action) => {

                state.total = action.payload.total

            },
            setShippingForm: (state, action) => {

                state.shipping_form = { ...action.payload.shipping_form }

            }
        }


    }
)



export default cartSlice.reducer;
export const { destroy, add, incrementQuantity, decrementQuantity, setQuantity, setTotal, setShippingForm } = cartSlice.actions;