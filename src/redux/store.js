import { configureStore } from "@reduxjs/toolkit";
import postsReducer from "./postsReducer";
import cartReducer from "./cartReducer";
import productsReducer from "./productsReducer";
import suppliersReducer from "./suppliersReducer";
import wasteReducer from "./wasteReducer";
import discountsReducer from "./discountsReducer";
import { apiSlice } from "./apiSlice";
import loadingReducer from "./loadingReducer";
import orderReducer from "./orderReducer";
import userReducer from "./userReducer";

export const store = configureStore({
	reducer: {
		cart: cartReducer,
		posts: postsReducer,
		products: productsReducer,
		suppliers: suppliersReducer,
		waste: wasteReducer,
		loading: loadingReducer,
		user: userReducer,
		discounts: discountsReducer,
		[apiSlice.reducerPath]: apiSlice.reducer,
		orders: orderReducer,
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware().concat(apiSlice.middleware),
});
