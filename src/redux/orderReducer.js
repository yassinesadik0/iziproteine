import { createSlice } from "@reduxjs/toolkit";
import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";




export const getOrders =createAsyncThunk("data/getOrders", async ()=>{
    try {
		let response = await axios.get(
			"http://localhost:5173/src/redux/orders.json"
		);
		let data = await response.data;
		return data;
	} catch (err) {
		return err.message;
    }
})


const OrderSlice = createSlice({
    name:"order",
    initialState:{
        orders:[],
        status:"idle",
        error:null
    },
    reducers:{},
    extraReducers: (builder)=>{
        builder
        .addCase(getOrders.pending, (state) => {
            state.status = "loading";
        })
        .addCase(getOrders.fulfilled, (state, action) => {
            state.status = "succeeded";
            state.orders = action.payload;
        })
        .addCase(getOrders.rejected, (state, action) => {
            state.status = "failed";
            state.error = action.error.message;
        });
    }

}

)



export default OrderSlice.reducer;