import { createSlice } from "@reduxjs/toolkit"

const loadingSlice  = createSlice({
	name: "loading",
	initialState: {
		value: "block"
	},
	reducers: {
		loading(state) {
			state.value = "block"
		},
		notLoading(state) {
			state.value = "hidden"
		}
	}
})

export default loadingSlice.reducer;
export const { loading, notLoading } = loadingSlice.actions;