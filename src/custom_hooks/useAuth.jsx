import React, { useState, useEffect } from "react";
import { usePostTokenMutation } from "../redux/apiSlice";

const useAuth = () => {
	const [authorized, setAuthorized] = useState("")
	const [user, setUser] = useState(null)
	const [isLoading, setIsLoading] = useState(true);
	const [postToken] = usePostTokenMutation();

	useEffect(() => {
		let token = localStorage.getItem("authToken")
		postToken(token)
			// .then(response => setUser(response.data.user))
			.then(response => setAuthorized(response.data.status))
			.then(response => setIsLoading(false))
			.catch(error => console.log(error.data))
	}, [])
	return { isLoading, authorized }
}

export default useAuth;