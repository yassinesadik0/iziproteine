import React from "react";
import Home from "../pages/Home";
import Products from "../pages/Products";
import { createBrowserRouter } from "react-router-dom"
import Layout from "../pages/client/Layout";
import GuestLayout from "../pages/guest/GuestLayout";
import Login from "../pages/guest/Login";
import SignUp from "../pages/guest/SignUp";
import Blogs from "../pages/Blogs";
import SingleBlog from "../components/blog/SingleBlog";
import AdminLayout from "../pages/admin/AdminLayout";
import AdminHome from "../pages/admin/AdminHome";
import AdminWaste from "../pages/admin/AdminWaste";
import AdminDiscounts from "../pages/admin/AdminDiscounts";
import AdminRoles from "../pages/admin/AdminRoles";
import AdminProducts from "../pages/admin/AdminProducts";
import AdminBlog from "../pages/admin/AdminBlog";
import AdminWasteTypes from "../pages/admin/AdminWasteTypes";
import AdminUsers from "../pages/admin/AdminUsers";
import PaymentLayout from "../pages/payment-process/PaymentLayout";
import FirstStep from "../pages/payment-process/FirstStep";
import SecondStep from "../pages/payment-process/SecondStep";
import AdminOrders from "../pages/admin/AdminOrders";
import AdminShowOrder from "../pages/admin/AdminShowOrder";
import AdminVariants from "../pages/admin/AdminVariants";
import WithAuth from "../components/WithAuth";
import Profile from "../components/Profile";

export const router = createBrowserRouter([
	{
		path: "/",
		element: <Layout />,
		children: [
			{
				path: "/",
				element: <Home />
			},
			{
				path: "/products",
				element: <Products />
			},
			{
				path: "/blog",
				element: <Blogs />
			},
			{
				path: "/blog/:id",
				element: <SingleBlog />
			},


			// {
			// 	path: "/contact",
			// 	element: <Contact  />
			// },
		]
	},
	{
		path: "/",
		element: <GuestLayout />,
		children: [
			{
				path: "/login",
				element: <Login />
			},
		]
	},
	{
		path: "/payment",
		element: <PaymentLayout />,
		children: [
			{
				path: "/payment/step-1",
				element: <FirstStep />
			},
			{
				path: "/payment/step-2",
				element: <SecondStep />
			},
		]
	},
	{
		element: <WithAuth />,
		children: [
			{
				path: "/admin",
				element: <AdminLayout />,
				children: [
					{
						path: "/admin",
						element: <AdminHome />,
					},
					{
						path: "/admin/variants",
						element: <AdminVariants />,
					},
					{
						path: "/admin/variants/:id/edit",
						element: <AdminVariants />,
					},
					{
						path: "/admin/products",
						element: <AdminProducts />,
					},
					{
						path: "/admin/products/:id/edit",
						element: <AdminProducts />,
					},
					{
						path: "/admin/blog",
						element: <AdminBlog />,
					},
					{
						path: "/admin/blog/:id/edit",
						element: <AdminBlog />,
					},
					{
						path: "/admin/users",
						element: <AdminUsers />,
					},
					{
						path: "/admin/users/:id/edit",
						element: <AdminUsers />,
					},
					{
						path: "/admin/roles",
						element: <AdminRoles />,
					},
					{
						path: "/admin/roles/:id/edit",
						element: <AdminRoles />,
					},
					{
						path: "/admin/wasteTypes",
						element: <AdminWasteTypes />,
					},
					{
						path: "/admin/wasteTypes/:id/edit",
						element: <AdminWasteTypes />,
					},
					{
						path: "/admin/waste",
						element: <AdminWaste />,
					},
					{
						path: "/admin/waste/:id/edit",
						element: <AdminWaste />,
					},
					{
						path: "/admin/orders",
						element: <AdminOrders />,
					},
					{
						path: "/admin/orders/:id",
						element: <AdminShowOrder />,
					},
					{
						path: "/admin/profile",
						element: <Profile />,
					},
				]
			},
			{
				path: "/",
				element: <Layout />,
				children: [
					{
						path: "/profile",
						element: <Profile />,

					}
				],
			},
		]
	},
	{
		path: "*",
		element: <h1>Not found</h1>,
	}

])
