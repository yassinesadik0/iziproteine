import React, { useEffect } from "react";
import Header from "../../components/Header";
import { Outlet } from "react-router-dom";
import Footer from "../../components/Footer";
import { useDispatch, useSelector } from "react-redux";
import { notLoading } from "../../redux/loadingReducer";
import { useGetProductsQuery } from "../../redux/apiSlice";
import useAuth from "../../custom_hooks/useAuth";

import { ToastContainer } from "react-toastify"

export default function Layout() {
	const dispatch = useDispatch()
	const { isLoading, authorized, user } = useAuth()

	useEffect(() => {
		window.onload = function () {
			dispatch(notLoading())
		}

	}, [])



	return (
		<>
			{/* {
				isLoading ? "loading" : user ? console.log(user) : console.log("nothing")
			} */}
			<ToastContainer />
			<Header />
			<Outlet />
			<Footer />
		</>
	)
}
