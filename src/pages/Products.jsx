import React ,{useEffect} from "react";
import Hero from "../components/products/hero";
import ProductSection from "../components/products/productSection";
import {useSelector, useDispatch} from "react-redux"
import { getProducts } from "../redux/productsReducer";
import { useGetProductsQuery } from "../redux/apiSlice";
import {toast} from "react-toastify";



export default function Products(props){

    const {data:products,isSuccess,
		isLoading,
		isFetching,
		isError,
		error} = useGetProductsQuery();
    

    return(<>
            <Hero/>
            {isSuccess  &&
                products.map((e,index)=>{console.log(e);return <ProductSection name={e.name} id={e.id} key={index} variants={e.variants} description={e.description} stock={e.stock} reviews={e.reviews} photos={e.photos} reversed={(index%2===0)?false:true} />})
            
            }
            
    </>)
}