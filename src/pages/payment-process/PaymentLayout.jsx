import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Outlet } from "react-router-dom"
import { ToastContainer } from "react-toastify";
import logo from "../../assets/hero/log.svg"
import arrow from "../../assets/icons/arrow-circle-broken-right.svg"
import { useSelector } from "react-redux";

export default function PaymentLayout() {
    const nav = useNavigate()
    const items = useSelector(state=>state.cart.items)

    useEffect(()=>{
        if (items.length < 1) {
            nav("/products")
        }
    },[])
    return (
        <>
            <div className="h-screen w-full relative">
                <ToastContainer />
                <div className="flex lg:flex-1 w-screen justify-start p-5">
                    <Link to="/" className="-m-1.5 p-1.5">
                        <span className="sr-only">Your Company</span>
                        <img className="h-8 md:h-12" src={logo} alt="Izi Proteine logo" />
                    </Link>
                <button 
                    type="button"
                    className="font-medium p-2 absolute right-0 justify-center items-center flex flex-row gap-2 text-primary-500 hover:text-primary-900 mr-3"
                    onClick={e => nav("/products")}
                    
                >
                    Continue Shopping
                    <img src={arrow} alt="" />
                </button>

                </div>
                
                <Outlet />
            </div>
        </>


    )
}