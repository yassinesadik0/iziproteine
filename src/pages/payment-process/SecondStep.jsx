import React, { useEffect, useState } from "react";
import Input from "../../components/payment/Input"
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setShippingForm } from "../../redux/cartReducer";
import axios from "axios";
import { toast } from "react-toastify";
import { usePostOrderMutation } from "../../redux/apiSlice";


export default function SecondStep() {

	const dispatch = useDispatch()

	const nav = useNavigate()

	const [postOrder, { isLoading, isSuccess, isError, error }] = usePostOrderMutation()

	const total = useSelector(state => state.cart.total);
	const shipping_form = useSelector(state => state.cart.shipping_form);
	const items = useSelector(state => state.cart.items);
	const [number, setNumber] = useState(shipping_form.phone)
	const [discount, setDiscount] = useState(shipping_form.phone)

	const onchange = (e) => {
		setNumber(e.target.value)
	}


	const onclickHandle = () => {
		if (number.length < 10) {
			toast.error("Please enter a valid phone number")
		} else {
			dispatch(setShippingForm({ ...shipping_form, phone: number }))
			postOrder({ ...shipping_form, phone: number, items }).then((res) => {;toast.success("order got placed successfully ! ");setTimeout(()=>nav("/"), 1000);; }).catch((err) => {console.log(err)})
		}
	}

	const onchangeDiscount = (e) => {

	}

	onclickHandle

	useEffect(() => {
		// ("this is the error")
		if (!total || !shipping_form.phone) {
			nav("/payment/step-1");
		}

	}, [])


	return (
		<div className="flex flex-col justify-center items-center h-3/4">
			<h1 className="text-3xl">Payment Method</h1>
			{/* cash on delivery  */}
			<div class={`h-auto w-80 bg-gradient-to-tr from-secondary-300 to-secondary-200 rounded-md text-gray-500 p-6 mt-5 `}>
				<p class="text-xl font-semibold">Cash On Delivery</p>
				<div class="input_text mt-6 relative">Please Correct your phone number to verify your order. We will call you shortly to confirm the order. </div>
				<div class="input_text mt-8 relative"> <Input type="text" onChange={e => onchange(e)} placeholder="Telephone" value={number} /> <span class="absolute left-0 text-sm -top-5">Telephone</span></div>
				<div class="mt-8 flex flex-col gap-5 ">
					<div class="input_text relative w-full">You can pay cash when your order is delivered.</div>
					<div class="input_text relative w-full">If you have any questions, please don't hesitate to reach us at 0657382947.</div>
				</div>
				<p class="text-lg text-center mt-4 text-gray-600 font-semibold">Payment amount: {total}DH</p>
				<div class="flex justify-center mt-4"> <button onClick={onclickHandle} class="outline-none pay h-12 bg-orange-600 text-white mb-3 hover:bg-orange-700 rounded-lg w-1/2 cursor-pointer transition-all">Confirm</button> </div>
			</div>


		</div>
	)
}