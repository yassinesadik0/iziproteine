import React, { useEffect, useState } from "react";
import Input from "../../components/payment/Input";

import { motion } from "framer-motion"

import Item from "../../components/cart/item";
import { useSelector, useDispatch } from "react-redux";
import { getProducts } from "../../redux/productsReducer";
import { useNavigate } from "react-router-dom";
import { destroy, setShippingForm, setTotal } from "../../redux/cartReducer";

import { toast } from "react-toastify";
import { useGetProductsQuery } from "../../redux/apiSlice";



export default function FirstStep() {
	const nav = useNavigate()

	const [isFirst, setIsFirst] = useState(true);

	const shipping_form = useSelector(state => state.cart.shipping_form)

	const [formData, setFormData] = useState({
		first_name: "",
		last_name: "",
		email: "",
		phone: "",
		zip: "",
		city: "",
		address: ""
	})


	const [invalidInputs, setInvalidInputs] = useState({
		first_name: false,
		last_name: false,
		email: false,
		phone: false,
		zip: false,
		city: false,
		address: false
	})




	const onchangeInput = (e) => { setFormData({ ...formData, [e.currentTarget.name]: e.currentTarget.value }); setInvalidInputs({ ...invalidInputs, [e.currentTarget.name]: false }); console.log(formData) }


	const items = useSelector((state) => state.cart.items)

	const dispatch = useDispatch()

	const shippingCost = "Free"


	const onSubmitValidation = (e) => {
		console.log(shipping_form)
		e.preventDefault()
		setIsFirst(false)
		let isEmpty = false;
		Object.entries(invalidInputs).forEach(([key, value]) => {
			if (value) {
				isEmpty = true
			}
		})


		if (isEmpty) {
			toast.error("all fields are required !")
		} else {
			dispatch(setTotal({ total: Number(items.reduce((total, e) => total + e.price * e.quantity, 0).toFixed(2))}))
			dispatch(setShippingForm({ shipping_form: { ...formData } }))
			nav("/payment/step-2");
		}
	}

	useEffect(
		() => {
			dispatch(getProducts())

			const newInvalidInputs = {};
			Object.entries(formData).forEach(([key, value]) => {
				if (value === "") {
					newInvalidInputs[key] = true;
				} else {
					newInvalidInputs[key] = false;
				}
			});
			setInvalidInputs(newInvalidInputs);


		}, [formData]);
	return (<>
		<h1 className="text-xl text-center font-poppins font-bold w-full ">Shipping Information</h1>
		<div className="flex h-5/6 gap-10 p-5 flex-col md:flex-row-reverse ">
			<div className=" flex justify-center items-center w-full md:w-1/2 border ">
				<div className="flex h-full flex-col overflow-y-scroll w-full bg-white shadow-xl">
					<div className="flex-1 overflow-y-auto py-6 px-4 sm:px-6">
						<div className="flex items-start justify-between">
							<h2
								className="text-lg font-medium text-gray-900"
								id="slide-over-title"
							>
								Shopping cart
							</h2>

						</div>
						<div className={`mt-8  ${items.length === 0 ? "h-[80%] flex justify-center items-center" : ""}`}>
							<div className="flow-root">
								{
									(items.length !== 0) ?

										<ul role="list" className="-my-6 divide-y divide-gray-200">
											{
												items.map((e, index) => (
													<li key={index} className="flex py-6">
														<Item key={index} id={e.id} name={e.name} price={e.price} quantity={e.quantity} image={e.image} variant={e.variant} stock={150} handleRemove={() => { dispatch(destroy({ id: e.id })) }} />
													</li>

												))

											}

										</ul>

										:

										nav("/")
								}
							</div>
						</div>
					</div>
					<div className="border-t border-gray-200 py-6 px-4 sm:px-6">
						<div className="flex justify-between text-base font-medium text-gray-900">
							<p>Subtotal</p>
							<p>{items.reduce((total, e) => total + e.price * e.quantity, 0).toFixed(2)} DH</p>
						</div>
						<div className="flex justify-between text-base font-light text-gray-900">
							<p>Shipping Cost</p>
							<p>{shippingCost}</p>
						</div>
						<div className="flex justify-between text-base font-medium text-gray-900 border-t-2 mt-2 pt-2">
							<p>Total</p>
							<p>{items.reduce((total, e) => total + e.price * e.quantity, 0).toFixed(2)} DH</p>
						</div>

					</div>
				</div>
			</div>

			<div className="flex justify-center w-full md:w-1/2 items-center m-auto p-5 ">
				<form className="w-full flex gap-2 flex-col" onSubmit={onSubmitValidation} method="post">
					<div className="relative z-0 w-full mb-6 group">
						<Input
							value={formData.first_name}
							error={!isFirst ? invalidInputs.first_name : false}
							onChange={onchangeInput}
							name={"first_name"}
							type={"text"}
							placeholder={"First Name"}
						/>
						<label
							className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-1 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
						>
							First Name
						</label>
					</div>
					<div className="relative z-0 w-full mb-6 group">
						<Input
							value={formData.last_name}
							error={!isFirst ? invalidInputs.last_name : false}
							onChange={onchangeInput}
							name={"last_name"}
							type={"text"}
							placeholder={"Last Name"}

						/>
						<label
							className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-1 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
						>
							Last Name
						</label>
					</div>
					<div className="relative z-0 w-full mb-6 group">
						<Input
							value={formData.email}
							error={!isFirst ? invalidInputs.email : false}
							onChange={onchangeInput}
							name={"email"}
							type={"email"}
							placeholder={"Email"}


						/>
						<label
							className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-1 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
						>
							Email
						</label>
					</div>
					<div className="grid md:grid-cols-2 md:gap-6">
						<div className="relative z-0 w-full mb-6 group">
							<Input
								value={formData.phone}
								error={!isFirst ? invalidInputs.phone : false}
								onChange={onchangeInput}
								name={"phone"}
								type={"phone"}
								placeholder={"Phone"}


							/>
							<label
								className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-1 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
							>
								Phone
							</label>
						</div>
						<div className="relative z-0 w-full mb-6 group">
							<Input
								value={formData.city}
								error={!isFirst ? invalidInputs.city : false}
								onChange={onchangeInput}
								name={"city"}
								type={"text"}
								placeholder={"City"}


							/>
							<label
								className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-1 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
							>
								City
							</label>
						</div>
					</div>
					<div className="grid md:grid-cols-2 md:gap-6">
						<div className="relative z-0 w-full mb-6 group">
							<Input
								value={formData.address}
								error={!isFirst ? invalidInputs.address : false}
								onChange={onchangeInput}
								name={"address"}
								type={"address"}
								placeholder={"Address"}


							/>
							<label
								className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-1 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
							>
								Address
							</label>
						</div>
						<div className="relative z-0 w-full mb-6 group">
							<Input
								value={formData.zip}
								error={!isFirst ? invalidInputs.zip : false}
								onChange={onchangeInput}
								name={"zip"}
								type={"number"}
								placeholder={"ZIP Code"}


							/>
							<label
								className="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-1 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"
							>
								Zip Code
							</label>
						</div>

					</div>
					<div className="w-full">
						<button type="submit" className="w-full bg-secondary-300 p-3 font-bold text-white rounded-md hover:bg-secondary-400">Continue</button>
					</div>
				</form>

			</div>


		</div></>)
}