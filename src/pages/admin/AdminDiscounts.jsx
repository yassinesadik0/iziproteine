import React from 'react'
import Table from '../../components/Table'
import CreateDiscount from '../../components/form/CreateDiscount'
import { useGetDiscountsQuery } from '../../redux/apiSlice'


function AdminDiscounts() {

	const {
		data: discounts,
		isSuccess,
		isLoading,
		isFetching,
		isError,
		error
	} = useGetDiscountsQuery();

	return (
		<div>
			<CreateDiscount />
			{isSuccess && <Table data={discounts} execluded={["address"]} />}
		</div>
	)
}

export default AdminDiscounts
