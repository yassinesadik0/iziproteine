import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { getOrders } from '../../redux/orderReducer'
import { useGetOrdersQuery, usePutOrderStatusMutation } from '../../redux/apiSlice'
import Bag from "../../assets/products/dummy-product.png"

function AdminShowOrder() {
	const { id } = useParams()
	const nav = useNavigate()
	const { data: orders, isSuccess, refetch, isLoading: isLoadingGetOrders } = useGetOrdersQuery(localStorage.getItem("authToken"))
	const [order, setOrder] = useState({})
	const [update, { isSuccesss, isLoading, isError, data: paload }] = usePutOrderStatusMutation()

	const onClickUpdate = async (credentials) => {
		await update(credentials);
		if (isSuccess) {
			refetch().then(() => {
				nav("/admin/orders")
			})
		}

	}

	useEffect(() => {
		console.log(isSuccess);
		if (orders && isSuccess) {
			setOrder(orders.find(e => e.id == id))
			console.log(order)
		}
	}, [orders, isSuccess])

	return (
		<div className='w-full flex justify-center items-center'>
			<a class="block w-1/2 m-6 p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
				<h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Order Id : {id}</h5>
				{
					isSuccess && order &&
					Object.keys(order).map((e, index) => {
						if (e === "products" || e === "total" || e == "items") {
							return "";
						}
						if (e === "client") {
							return <><p className=' font-poppins font-bold  text-xl text-gray-800 dark:text-gray-400 mt-3'>Client :</p><hr className='border border-gray-600 m-3' />{Object.keys(order[e]).map(ele => <p class="font-normal text-gray-700 dark:text-gray-400"><span className='font-semibold font-poppins'>{ele}</span>{" : " + order[e][ele]}</p>)}<hr className='border border-gray-600 m-3' /></>
						}

						return <p class="font-normal text-gray-700 dark:text-gray-400"><span className='font-semibold font-poppins'>{e}</span> {" : " + order[e]}</p>
					})
				}
				{
					isLoadingGetOrders && <h1>Loading...</h1>
				}
				<div className='mt-5'>
					{
						isSuccess && order &&
							(isLoading || isLoadingGetOrders) ? <h1>Loading...</h1> : (
							order.status != "confirmed" ?
								<>
									<Link
										onClick={e => { onClickUpdate({ id: order.id, data: { status: "confirmed", delivery: "in progress" } }) }}
										className='bg-secondary-400 p-3 mt-3 mr-3 rounded-md text-white hover:bg-secondary-300 hover:text-gray-500'>
										Confirm
									</Link>
									<Link
										onClick={e => { onClickUpdate({ id: order.id, data: { reject: "true" } }) }}
										className='bg-danger-600 p-3 mt-3 mr-3 rounded-md text-white hover:bg-danger-500 hover:text-gray-500'>
										Reject
									</Link>
								</> : (order.delivery == "delivered" ?
									<h1 className='font-extrabold text-xl text-success-500'>
										Delivered !
									</h1>
									:
									<>
										<Link
											onClick={e => { onClickUpdate({ id: order.id, data: { status: "confirmed", delivery: "delivered" } }) }}
											className='bg-secondary-400 p-3 mt-3 mr-3 rounded-md text-white hover:bg-secondary-300 hover:text-gray-500'>
											Delivered
										</Link>
										<Link
											onClick={e => { onClickUpdate({ id: order.id, data: { reject: "true" } }) }}
											className='bg-danger-600 p-3 mt-3 mr-3 rounded-md text-white hover:bg-danger-500 hover:text-gray-500'>
											Returned
										</Link>
									</>)

						)

					}
				</div>
			</a>
			<a class="block w-1/2 m-6 p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
				<span className='font-bold w-full text-xl'>Panier : </span>
				{
					isSuccess && order.items &&
					order.items.map((e) => <div

						className='flex flex-row mt-3'

					>
						<div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
							<img
								src={Bag}
								className="h-full w-full object-cover object-center"

							/>
						</div>
						<div
							className="ml-4 flex flex-1 flex-col"
						>
							<div>
								<div className="flex justify-between text-base font-medium text-gray-900">
									<h3>
										<a href="#">{e.product.name}</a>
									</h3>
									<p className="ml-4">{e.variant.price} DH</p>
								</div>
							</div>
							<p className="text-gray-600 pt-3">{e.variant.size}</p>
							<div className="flex flex-1 items-end justify-between text-sm">
								<p className="text-gray-500">Qty {e.quantity}</p>
							</div>
						</div>
					</div>)
				}
				<hr className='w-2/3 border  border-gray-700 opacity-50 mx-auto m-5' />
				<div className='flex flex-row  font-bold font-poppins'>
					<h1 className='w-1/2 text-left'>Total</h1>
					<p className='w-1/2 text-right'>{order.total} DH</p>
				</div>
			</a>
		</div>
	)
}

export default AdminShowOrder