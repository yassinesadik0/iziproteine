import React from 'react'
import Table from '../../components/Table'
import { useGetRolesQuery } from '../../redux/apiSlice'
import { useParams } from 'react-router-dom'
import CreateRole from '../../components/form/CreateRole'


function AdminRoles() {
	const { id } = useParams()

	const {
		data: roles,
		isLoading,
		isSuccess,
		isError,
		error,
		refetch
	} = useGetRolesQuery()

	const handleRefetch = () => {
		refetch({ force: true })
	}
	return (
		<div>
			<CreateRole  onRefetch={handleRefetch} id={id} edit={id ? true : false} />
			{
				isSuccess && <Table onRefetch={handleRefetch} data={roles} table={"roles"} uri={"roles"} execluded={[]} />
			}
		</div>
	)
}

export default AdminRoles
