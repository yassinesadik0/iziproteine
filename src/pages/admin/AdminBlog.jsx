import React from 'react'
import CreateBlog from '../../components/form/CreateBlog'
import Table from '../../components/Table'
import { useGetPostsQuery } from '../../redux/apiSlice'
import { useParams } from 'react-router-dom'

function AdminBlog() {
	const { id } = useParams()
	const {
		data: posts,
		isSuccess,
		isLoading,
		isFetching,
		isError,
		error,
		refetch
	} = useGetPostsQuery();
	
	const handleRefetch = () => {
		refetch({ force: true })
	}
	return (
		<div>
			<CreateBlog onRefetch={handleRefetch} id={id} edit={id ? true : false}  />
			{
				isSuccess && <Table onRefetch={handleRefetch} data={Array.from(posts)}  table={"posts"} uri={"blog"} execluded={["photo", 'content']} />
			}
		</div>
	)
}

export default AdminBlog
