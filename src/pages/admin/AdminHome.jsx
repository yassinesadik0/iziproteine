import React from 'react'
import CustomButtonMotion from '../../components/CustomButtonMotion'

function AdminHome() {
	return (
		<div className="bg-gray-100">
			<div className="container mx-auto flex flex-col items-center py-12 sm:py-24">
				<div className="w-11/12 sm:w-2/3 lg:flex justify-center items-center flex-col  mb-5 sm:mb-10">
					<h1 className="text-2xl sm:text-3xl md:text-4xl lg:text-5xl xl:text-6xl text-center text-secondary-700 font-black leading-7 md:leading-10">
						Welcome to your
						<span className="text-gradient to-right"> Dashboard</span>
						!
					</h1>
					<p className="mt-5 sm:mt-10 lg:w-10/12 text-secondary-500 font-normal text-center text-sm sm:text-lg">
						We're thrilled to have you here. This comprehensive platform is designed to provide you with real-time insights, analytics, and data visualization to help you make informed decisions and track key metrics.{" "}
					</p>
				</div>
				<div className="flex justify-center items-center">
					<CustomButtonMotion
						text={"See profile"}
						to={"/admin/profile"}
						classNames={"flex justify-center items-center w-fit h-8 px-8 py-5 md:w-52 md:h-14 md:mt-8 md:px-7 md:py-3  font-semibold text-white md:font-bold text-xs md:text-sm lg:text-xl md:rounded-xl rounded-lg transition duration-150 ease-in-out cursor-pointer"}
					/>
				</div>
			</div>
		</div>
	)
}

export default AdminHome
