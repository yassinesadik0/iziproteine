import React, { useEffect, useState } from 'react'
import OrdersTable from '../../components/OrdersTable'
import { useGetOrdersQuery } from '../../redux/apiSlice'
import { useDispatch } from 'react-redux'




function AdminOrders() {
	const { data: orders, isSuccess, refetch, isLoading } = useGetOrdersQuery(localStorage.getItem("authToken"))
	const [loadingAfterRefetch, setLoadingAfterRefetch] = useState(false)

	const handleRefetch = () => {
		setLoadingAfterRefetch(true)
		refetch({ force: true }).then(() => {
			setLoadingAfterRefetch(false)
		}
		)
	}

	useEffect(() => {
		console.log("success", isSuccess, "loading", isLoading);
	}, [isLoading])

	return (<>
		{loadingAfterRefetch?
		<h1>Loading...</h1>
			:<div>
				{
					isSuccess && <OrdersTable onRefetch={handleRefetch} data={orders} />
				}
			</div>
		}
	</>
	)
}

export default AdminOrders
