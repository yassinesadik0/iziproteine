import React from 'react'
import { Outlet, useLocation, useNavigate } from 'react-router-dom'
import AdminSideBar from '../../components/admin/AdminSideBar'
import AdminSubHeader from '../../components/admin/AdminSubHeader'
import Footer from "../../components/Footer"
import { useEffect } from 'react'



function AdminLayout() {
	const location = useLocation();
	
	const navigate = useNavigate();


	useEffect(() => {
		if (localStorage.getItem("user") === null) {
			

			navigate("/login")


		}	
	}, [])

	
	return (
		<>
			<AdminSideBar />
			<div className="relative md:ml-64 bg-bgGray">
				{
					location.pathname === "/admin/profile" ? undefined : <AdminSubHeader />
				}
				<Outlet />
				<Footer />
			</div>
		</>
	)
}

export default AdminLayout
