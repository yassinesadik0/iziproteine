import React from 'react'
import Table from '../../components/Table'
import CreateWaste from '../../components/form/CreateWaste'
import { useGetWasteQuery } from '../../redux/apiSlice'
import { useParams } from 'react-router-dom'


function AdminWaste() {
	const { id } = useParams()
	const {
		data: waste,
		isSuccess,
		isLoading,
		isFetching,
		isError,
		error,
		refetch
	} = useGetWasteQuery();

	const handleRefetch = () => {
		refetch({ force: true })
	}
	
	return (
		<div>
			<CreateWaste onRefetch={handleRefetch} id={id} edit={id ? true : false} />
			{
				isSuccess && <Table onRefetch={handleRefetch} data={waste} table={"wastes"} uri={"waste"} execluded={[]} />
			}
		</div>
	)
}

export default AdminWaste
