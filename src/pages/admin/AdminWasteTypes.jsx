import React from 'react'
import Table from '../../components/Table'
import { useGetWasteTypesQuery } from '../../redux/apiSlice'
import { useParams } from 'react-router-dom'
import CreateWasteType from '../../components/form/CreateWasteType'


function AdminWasteTypes() {
	const { id } = useParams()

	const {
		data: wasteTypes,
		isLoading,
		isSuccess,
		isError,
		error,
		refetch
	} = useGetWasteTypesQuery()

	const handleRefetch = () => {
		refetch({ force: true })
	}

	return (
		<div>
			<CreateWasteType onRefetch={handleRefetch} id={id} edit={id ? true : false} />
			{
				isSuccess && <Table onRefetch={handleRefetch} data={wasteTypes} table={"wasteTypes"} uri={"wasteTypes"} execluded={[]} />
			}
		</div>
	)
}

export default AdminWasteTypes
