import React from 'react'
import CreateProduct from '../../components/form/CreateProduct'
import Table from '../../components/Table'
import { useGetVariantsQuery } from '../../redux/apiSlice'
import { useParams } from 'react-router-dom'
import EditVariant from '../../components/form/EditVariant'


function AdminVariants() {
	const { id } = useParams()

	const {
		data: variants,
		isLoading,
		isSuccess,
		isError,
		error,
		refetch
	} = useGetVariantsQuery()

	const handleRefetch = () => {
		refetch({ force: true })
	}
	return (
		<div>
			{
				id ? <EditVariant onRefetch={handleRefetch} id={id} /> : undefined
			}
			

			{isLoading ? <p>Loading...</p>: ""}			
			{isSuccess && <Table onRefetch={handleRefetch} data={variants} uri={"variants"} table={"productVariants"} products={false} execluded={[""]} />}
		</div>
	)
}

export default AdminVariants
