import React from 'react'
import CreateProduct from '../../components/form/CreateProduct'
import Table from '../../components/Table'
import { useGetProductsQuery } from '../../redux/apiSlice'
import { useParams } from 'react-router-dom'


function AdminProducts() {
	const { id } = useParams()

	const {
		data: products,
		isLoading,
		isSuccess,
		isError,
		error,
		refetch
	} = useGetProductsQuery()

	const handleRefetch = () => {
		refetch({ force: true })
	}

	return (
		<div>
			<CreateProduct onRefetch={handleRefetch} create={true} id={id} edit={id ? true : false} />

			{isLoading ? <p>Loading...</p> : ""}
			{isSuccess && <Table onRefetch={handleRefetch} data={products} table={"products"} uri={"products"} products={true} execluded={["description", "photos"]} />}
		</div>
	)
}

export default AdminProducts
