import React from 'react'
import Table from '../../components/Table'
import CreateUser from '../../components/form/CreateUser'
import { useGetUsersQuery } from '../../redux/apiSlice'
import { useParams } from 'react-router-dom'

function AdminUsers() {
	const { id } = useParams()
	const {
		data: users,
		isSuccess,
		isLoading,
		isFetching,
		isError,
		error,
		refetch,
	} = useGetUsersQuery();

	const handleRefetch = () => {
		refetch({ force: true })
	}

	return (
		<div>
			<CreateUser onRefetch={handleRefetch} id={id} edit={id ? true : false} />
			{
				isSuccess && <Table onRefetch={handleRefetch} data={users} table={"users"} uri={"users"} execluded={["photo", "address", 'password']} />
			}
		</div>
	)
}

export default AdminUsers
