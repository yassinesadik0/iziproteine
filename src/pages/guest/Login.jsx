import { Link, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { usePostLoginMutation } from "../../redux/apiSlice";
import { ToastContainer, toast } from "react-toastify";

function Login() {

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [loginResponse, setLoginResponse] = useState({
		token: "2394587djhksfh3274",
		user: {
			id: 1,
			name: "Izi Proteine",
			role: "admin",
		},
		status: "authorized",
	})
	const navigate = useNavigate();


	const handleSubmit = (e) => {
		e.preventDefault()
		// console.log(credentials)
		if (email === "iziproteine@gmail.com" && password === "Iziproteine123") {
			localStorage.setItem("authToken", loginResponse.token)
			localStorage.setItem("user", JSON.stringify(loginResponse.user))
			navigate("/admin")
		} else {
			toast.error("Invalid credentials")
			navigate("/login")
		}
	}

	// useEffect(() => {
	// 	if (loginResponse.status === "authorized") {
	// 		localStorage.setItem("authToken", loginResponse.token)
	// 		localStorage.setItem("user", JSON.stringify(loginResponse.user))
	// 		if (loginResponse.user.role.toLowerCase() === "admin") {
	// 			navigate("/admin")
	// 		} else {
	// 			navigate("/")
	// 		}
	// 	}
	// 	if (loginResponse.status === "unauthorized") {
	// 		navigate("/login")
	// 	}
	// }, [])

	return (
		<>
		<ToastContainer />
			<div className="container mx-auto px-4 h-[700px]">
				<div className="flex content-center items-center justify-center h-full">
					<div className="w-full lg:w-4/12 px-4">
						<div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-slate-700 border-0 pt-10">
							<div className="flex-auto px-4 lg:px-10 py-10 pt-0">
								<div className="text-slate-300 text-center mb-3 text-lg font-bold">
									<small>Sign in with credentials</small>
								</div>
								<form onSubmit={handleSubmit}>
									<div className="relative w-full mb-3">
										<label
											className="block uppercase text-slate-100 text-xs font-bold mb-2"
											htmlFor="grid-password"
										>
											Email
										</label>
										<input
											type="email"
											className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
											name="email"
											placeholder="Email"
											onChange={(e) => setEmail(e.target.value)}
											value={email}
										/>
									</div>

									<div className="relative w-full mb-3">
										<label
											className="block uppercase text-slate-100 text-xs font-bold mb-2"
											htmlFor="grid-password"
										>
											Password
										</label>
										<input
											type="password"
											className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
											placeholder="Password"
											onChange={(e) => setPassword(e.target.value)}
											value={password}
										/>
									</div>
									<div>
										<label className="inline-flex items-center cursor-pointer">
											<input
												id="customCheckLogin"
												type="checkbox"
												className="form-checkbox border-0 rounded text-slate-400 ml-1 w-5 h-5 ease-linear transition-all duration-150"
											/>
											<span className="ml-2 text-sm font-semibold text-slate-300">
												Remember me
											</span>
										</label>
									</div>

									<div className="text-center mt-6">
										<button
											className="bg-slate-400 text-slate-700 active:bg-slate-300 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
											type="submit"
										>
											Sign In
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}

export default Login;