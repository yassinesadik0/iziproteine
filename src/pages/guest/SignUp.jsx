import { Link } from "react-router-dom";
import React from "react";
import google from "../../assets/img/google.svg"
import github from "../../assets/img/github.svg"


function SignUp() {
	return (
		<>
			<div className="container mx-auto px-4 h-fit py-10">
				<div className="flex content-center items-center justify-center h-full">
					<div className="w-full flex flex-col justify-center items-center px-4">
						<div className="relative flex flex-col min-w-0 break-words  mb-6 shadow-lg rounded-lg bg-slate-700 border-0 w-full lg:w-[600px]">
							<div className="rounded-t mb-0 px-6 py-6">
								<div className="text-center mb-3">
									<h6 className="text-slate-300 text-lg font-bold">
										Sign up with
									</h6>
								</div>
								<div className="btn-wrapper text-center">
									<button
										className="bg-slate-200 active:bg-slate-50 text-slate-700  px-4 py-2 rounded outline-none focus:outline-none mr-2 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs ease-linear transition-all duration-150"
										type="button"
									>
										<img
											alt="..."
											className="w-5 mr-1"
											src={github}
										/>
										Facebook
									</button>
									<button
										className="bg-slate-200 active:bg-slate-50 text-slate-700  px-4 py-2 rounded outline-none focus:outline-none mr-1 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs ease-linear transition-all duration-150"
										type="button"
									>
										<img
											alt="..."
											className="w-5 mr-1"
											src={google}
										/>
										Google
									</button>
								</div>
								<hr className="mt-6 border-b-1 border-blueGray-300" />
							</div>
							<div className="flex-auto px-4 lg:px-10 py-10 pt-0">
								<div className="text-slate-300 text-center mb-3 text-lg font-bold">
									<small>Or sign up with credentials</small>
								</div>
								<form>
									<div className="relative w-full mb-3">
										<label
											className="block uppercase text-slate-100 text-xs font-bold mb-2"
											htmlFor="grid-password"
										>
											First name
										</label>
										<input
											type="text"
											className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
											placeholder="First name"
										/>
									</div>
									<div className="relative w-full mb-3">
										<label
											className="block uppercase text-slate-100 text-xs font-bold mb-2"
											htmlFor="grid-password"
										>
											Last name
										</label>
										<input
											type="text"
											className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
											placeholder="Last name"
										/>
									</div>
									<div className="relative w-full mb-3">
										<label
											className="block uppercase text-slate-100 text-xs font-bold mb-2"
											htmlFor="grid-password"
										>
											Email
										</label>
										<input
											type="email"
											className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
											placeholder="Email"
										/>
									</div>

									<div className="relative w-full mb-3">
										<label
											className="block uppercase text-slate-100 text-xs font-bold mb-2"
											htmlFor="grid-password"
										>
											Password
										</label>
										<input
											type="password"
											className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
											placeholder="Password"
										/>
									</div>
									<div className="relative w-full mb-3">
										<label
											className="block uppercase text-slate-100 text-xs font-bold mb-2"
											htmlFor="grid-password"
										>
											Confirm password
										</label>
										<input
											type="password"
											className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
											placeholder="Confirm password"
										/>
									</div>
									<div>
										<label className="inline-flex items-center cursor-pointer">
											<input
												id="customCheckLogin"
												type="checkbox"
												className="form-checkbox border-0 rounded text-slate-400 ml-1 w-5 h-5 ease-linear transition-all duration-150"
											/>
											<span className="ml-2 text-sm font-semibold text-slate-300">
												I agree with the <Link to="/" className="text-slate-100">Privacy policy</Link>
											</span>
										</label>
									</div>

									<div className="text-center mt-6">
										<button
											className="bg-slate-400 text-slate-700 active:bg-slate-300 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
											type="button"
										>
											Sign Up
										</button>
									</div>
								</form>
							</div>
						</div>
						<div className="flex flex-wrap mt-2 relative">
							
							<div className="text-right">
								<Link to="/login" className="text-slate-700 text-lg font-semibold">
									<small>Or login</small>
								</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}

export default SignUp;