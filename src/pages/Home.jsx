import Features from "../components/home/Features";
import Hero from "../components/home/Hero";
import Stats from "../components/home/Stats";
import Products from "../components/home/Products";
import Posts from "../components/home/Posts";
import Team from "../components/home/Team";
import Contact from "../components/home/Contact";
import Testimonials from "../components/home/Testimonials";
// import { useSelector } from "react-redux";



export default function Home() {
	// let loading = useSelector(state => state.loading.value)

	return (
		<div >
			{/* <div className={`bg-danger-600 ${loading} w-full h-[100vh] absolute top-0 z-50`}>
			</div> */}
			<Hero />
			<Features />
			<Stats />
			<Products />
			<Posts />
			<Testimonials />
			<Team />
			<Contact />
		</div>
	)
}