import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import { RouterProvider } from 'react-router-dom'
import { router } from './utils/router'
import { Provider } from 'react-redux'
import { store } from './redux/store'
import 'react-toastify/dist/ReactToastify.css';


ReactDOM.createRoot(document.getElementById('root')).render(
	<React.StrictMode>
		<Provider store={store}>
			<RouterProvider router={router} >
				<App />
			</RouterProvider>
		</Provider>
	</React.StrictMode>,
)