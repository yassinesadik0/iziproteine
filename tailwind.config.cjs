/** @type {import('tailwindcss').Config} */

module.exports = {
	content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
	darkMode: "class",
	theme: {
		extend: {
			fontFamily: {
				poppins: ["Poppins", "sans-serif"],
			},
			screens: {
				mobile: "475px",
			},
			width: {
				250: "583px",
				heroImg: "35rem",
				txtWidth: "30rem",
				300: "40rem",
			},
			height: {
				hero: "30rem",
			},

			colors: {
				primary: {
					100: "#FDF1E5",
					200: "#FBE0CC",
					300: "#F3C7AF",
					400: "#E7AD97",
					500: "#D88974",
					600: "#B96254",
					700: "#9B403A",
					800: "#7D2525",
					900: "#67161D",
				},
				secondary: {
					100: "#D6F6EC",
					200: "#B0EDE0",
					300: "#7BCAC0",
					400: "#4D9592",
					500: "#1C4D4F",
					600: "#143E43",
					700: "#0E2F38",
					800: "#08232D",
					900: "#051925",
				},
				danger: {
					500: "#FF7F85",
					600: "#DB5C6E",
					700: "#B7405A",
					800: "#93284A",
					900: "#7A183F",
				},
				success: {
					500: "#66C794",
					600: "#4AAB82",
					700: "#338F71",
					800: "#207360",
					900: "#135F55",
				},
				info: {
					500: "#09C5E2",
					600: "#069AC2",
					700: "#0474A2",
					800: "#025383",
					900: "#013C6C",
				},
				warning: {
					500: "#f59e0b",
					600: "#d97706",
					700: "#b45309",
					800: "#92400e",
					900: "#78350f",
				},
				bgGray: "#E4E4E4",
				cardBg: "#D6D6D6",
			},
		},
	},
	plugins: [require("@tailwindcss/forms")],
};
